<?php
class XA_ListView_Photo_Thumb extends XA_ListView {

	public $option_list = array();
	public $base_param_list = array();
	public $pk_list = array();


	public function out() {
		parent::out();
		
		$output = '<ul class="thumb">';
		foreach ($this->row_list as $photo) {
			$option_list = array();
			foreach ($this->option_list as $option) {
				$param_list = $option['param_list'];
				foreach ($this->pk_list as $pk) {
					$param_list[$pk] = $photo[$pk];
				}
				$option_list[] = '<a class="button '.$option['class'].'" href="'.xa::url($param_list).'" title="'.$option['label'].'">'.$option['label'].'</a>';
			}
			$output .= '
				<li>
					<a class="thumb fancybox" rel="zdjecia" href="'.xa::url(array('action' => 'photo') + $photo).'" title="'.$photo['title'].'">
						<img id="image_'.$photo['file_id'].'" src="'.xa::url(array('action' => 'thumb') + $photo).'" alt="'.$photo['title'].'" />
					</a>
					<span class="buttons">
						'.implode("\n", $option_list).'
					</span>
				</li>
			';
		}
		return $output.'</ul>';
	}


	public function addOption($name, $label, $params) {
		$this->option_list[$name] = array('param_list' => $params, 'label' => $label, 'name' => $name, 'class' => $name);
	}
}
