<?php
class XA_ListView_Table extends XA_ListView {

	public $table;
	public $pk_list = array();
	public $base_param_list = array();
	public $option_list = array();
	public $filter_list = array();



	public function __construct($query = null) {
		parent::__construct($query);

		$this->table = new XA_XHTML_Table();
	}



	public function get_page_nav() {
		$pagenav = '';

		if ($this->page_count > 1) {
			$pagenav = '<div class="pagenav">Strona:<ul class="paging">';
			$param_list = $this->base_param_list;

			if ($this->page > 1) {
				$param_list['page'] = $this->page - 1;
				$pagenav .= '<li><a href="'.xa::url($param_list).'">&laquo; Poprzednia</a></li>';
			}

			for ($page_loop = 1; $page_loop <= $this->page_count; $page_loop++) {
				if (($this->page >= $page_loop - 10) && ($this->page <= $page_loop + 10)) {
					$param_list['page'] = $page_loop;
					$pagenav .= '<li class="'.(($page_loop == $this->page) ? 'active' : '').'"><a href="'.xa::url($param_list).'">'.$page_loop.'</a></li>';
				}
			}

			if ($this->page < $this->page_count) {
				$param_list['page'] = $this->page + 1;

				$pagenav .= '<li><a href="'.xa::url($param_list)
					.'">Następna &raquo;</a></li>';
			}

			$pagenav .= '</ul></div>';
		}

		return $pagenav;
	}



	public function out() {
		if ($this->query) {
			foreach ($this->filter_list as $filter) {
				$filter->modifyQuery($this->query);
			}
		}

		parent::out();
		$this->table->row_list =& $this->row_list;

		if (
			$this->option_list
			&&
			empty($this->table->field_list['options'])
		) {
			$this->table->addField(
				new XA_Table_Field_Options('options', 'Opcje', $this)
			);
		}

		$filter_bar = '';

		if ($this->filter_list) {
			$param_list = array_diff_key(
				  $this->base_param_list
				, array_flip(array('page'))
			);

			$hidden_params = '';

			foreach ($param_list as $name => $value) {
				$hidden_params .= "\n"
					.'<input type="hidden" '
					.'name="'.$name.'" value="'.$value.'" />';
			}

			$filter_bar .= '<div class="filters">
				<form action="'.xa::url($param_list).'" method="get">
					<div class="button_container">
						<span>'.xa::say('Filtry').'</span>
					</div>
					<div class="clear"></div>
					<p style="display: none;">
					'.$hidden_params.'
					</p>
			';

			foreach ($this->filter_list as $filter) {
				$filter_bar .= $filter->out();

				if (strlen($filter->value)) {
					$this->base_param_list[$filter->name] = $filter->value;
				}
			}

			$filter_bar .= '
					<div class="clear"></div>
					<p class="submit">
						<input type="submit" value="'.xa::say('Przefiltruj').'" />
						<a href="'.xa::url(array_intersect_key($this->base_param_list, array_flip(array('action')))).'">'
							.xa::say('Wyczyść filtry').'</a>
					</p>
				</form></div>';
		}

		$pagenav = $this->get_page_nav();

		return $filter_bar.$pagenav.$this->table->out().$pagenav;
	}



	public function addOption($name, $label, $params = null) {
		$this->option_list[$name] = array(
			  'param_list' => $params ?: array()
			, 'label' => $label
			, 'name' => $name
			, 'class' => $name
		);
	}



	public function addFilter($field_name, $filter) {
		$filter->field_name = $field_name;
		$this->filter_list[$field_name][] = $filter;
	}



	public function setLabels($label_list) {
		$field_name_list = array_keys($this->field_list);

		foreach ($field_label_list as $field_name => $label) {
			if (in_array($field_name, $field_name_list)) {
				$this->field_list[$field_name]['label'] = $label;
			}
		}
	}
}
