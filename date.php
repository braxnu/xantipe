<?php
namespace xa;

abstract class Date {

	public static $weekdays = array(
		  0 => 'Niedziela'
		, 1 => 'Poniedziałek'
		, 2 => 'Wtorek'
		, 3 => 'Środa'
		, 4 => 'Czwartek'
		, 5 => 'Piątek'
		, 6 => 'Sobota'
	);

	public static $weekdays_short = array(
		  0 => 'Nd'
		, 1 => 'Pn'
		, 2 => 'Wt'
		, 3 => 'Śr'
		, 4 => 'Czw'
		, 5 => 'Pt'
		, 6 => 'Sb'
	);

	public static $month_names_of = array(
		  1 => 'Stycznia'
		, 2 => 'Lutego'
		, 3 => 'Marca'
		, 4 => 'Kwietnia'
		, 5 => 'Maja'
		, 6 => 'Czerwca'
		, 7 => 'Lipca'
		, 8 => 'Sierpnia'
		, 9 => 'Września'
		, 10 => 'Października'
		, 11 => 'Listopada'
		, 12 => 'Grudnia'
	);

	public static $month_names = array(
		  1 => 'Styczeń'
		, 2 => 'Luty'
		, 3 => 'Marca'
		, 4 => 'Kwietnia'
		, 5 => 'Maj'
		, 6 => 'Czerwca'
		, 7 => 'Lipca'
		, 8 => 'Sierpnia'
		, 9 => 'Wrzesień'
		, 10 => 'Październik'
		, 11 => 'Listopad'
		, 12 => 'Grudnia'
	);


	public static function get($date, $time = false) {
		$date = strtotime($date);
		$year = date('Y', $date);
		$month_num = date('n', $date);
		$day = date('j', $date);
		$weekday_num = date('w', $date);

		$time = $time ? ', '.date('H:i', $date) : '';

		return
			self::$weekdays[$weekday_num]
			.' - '
			.$day
			.' '
			.self::$month_names_of[$month_num]
			.($year != date('Y') ? ' '.$year : '')
			.$time;
	}
}
?>
