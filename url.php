<?php
namespace xa;

class Url {

    private static $route_map = [];



    public static function redirect ($url = []) {
        if (is_array($url)) {
            $url = self::get($url, false);
        }

        header('Location: '.$url);
    }



    public static function get_wrapper ($param_list = []) {
        $is_secure = false;

        if (isset($param_list['_secure'])) {
            $is_secure = $param_list['_secure'];
        } elseif (isset($_SERVER['HTTPS'])) {
            $is_secure = $_SERVER['HTTPS'] === 'on';
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
            $is_secure = $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https';
        }

        return $is_secure ? 'https' : 'http';
    }



    public static function get_query ($param_list = [], $for_html = false) {
        if (!$param_list) {
            return '';
        }

        $query_param_list = [];

        // filter param names starting with '_'
        foreach ($param_list as $key => $value) {
            if (substr($key, 0, 1) !== '_') {
                $query_param_list[$key] = $value;
            }
        }

        return http_build_query(
            $query_param_list,
            '',
            ($for_html ? '&amp;' : '&')
        );
    }



    public static function get_base ($param_list = []) {
        if (!empty($param_list['_host'])) {
            $host = $param_list['_host'];
        } elseif (defined('\\MODULE_HOST')) {
            $host = \MODULE_HOST;
        } else {
            $host = \SITE_HOST;
        }

        if (!empty($param_list['_path'])) {
            $path = $param_list['_path'];
        } elseif (defined('\\MODULE_URL_PATH')) {
            $path = \MODULE_URL_PATH;
        } elseif (defined('\\BASE_PATH')) {
            $path = \BASE_PATH;
        } else {
            $path = '/';
        }

        return self::get_wrapper($param_list).'://'.$host.$path;
    }



	public static function get ($param_list = [], $for_html = false) {
        if (is_string($param_list)) {
            parse_str($param_list, $param_list);
        }

        if (!empty($param_list['action'])) {
            $action = $param_list['action'];

            if (!isset(self::$route_map[$action])) {
                if (file_exists(\MODULE_PATH.'routes/'.$action.'.php')) {
                    include(\MODULE_PATH.'routes/'.$action.'.php');
                } else {
                    self::$route_map[$action] = false;
                }
            }

            $generator = self::$route_map[$action];

            if ($generator) {
                return $generator($param_list, $for_html);
            }
        }


        $hash = empty($param_list['_hash']) ? '' : '#'.$param_list['_hash'];
        $query = self::get_query($param_list, $for_html);

        return
            self::get_base($param_list).
            ($query ? '?'.$query : '').
            $hash;
    }
}
