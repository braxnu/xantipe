<?php
namespace xa\user;

class DB extends \xa\User {

    protected $db;
    protected $session;



    public function __construct ($db = null) {
        $this->session =& $_SESSION[__CLASS__];

        if (!$db) {
            $db = \xa\DB::get();
        }

        $this->db = $db;
    }



    public function login ($id) {
        $this->session['user_id'] = $id;
    }



    public function logout () {
        $this->session = [];
    }



    public function get_id () {
        return
            empty($this->session['user_id'])
            ? null
            : $this->session['user_id'];
    }



    public function authen ($login, $password) {
        $sql = new \xa\db\Query('user');

        $sql
            ->select('user_id')

            ->where("login = '".$this->db->esc($login)."'")
            ->where("password = '".md5($password)."'");

        return intval( $this->db->query($sql)->val() );
    }



    public function get_data () {
        $sql = new \xa\db\Query('user');

        $sql
            ->where('user_id = '.$this->session['user_id'])
            ->limit(1);

        return $this->db->query($sql)->row();
    }
}
