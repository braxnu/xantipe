<?php
class XA_Db_Tree {


	public static function getChildIdList($table, $node_id_list, $parent_key = null, $primary_key = null) {
	
		if (!is_array($node_id_list)) {
			$node_id_list = array($node_id_list);
		}
	
		$parent_key = ($parent_key ? $parent_key : 'parent_id');
		$primary_key = ($primary_key ? $primary_key : $table.'_id');
		
		$child_id_list = xa::query("SELECT `".$primary_key."` FROM `".$table."` WHERE `".$parent_key."` IN (".($node_id_list ? implode(', ', $node_id_list) : 'NULL').")")->getValues($primary_key);
		
		if ($child_id_list) {
			$child_id_list = array_unique(array_merge(self::getChildIdList($table, $child_id_list, $parent_key, $primary_key), $child_id_list));
		}
		
		return array_unique(array_merge($node_id_list, $child_id_list));
	}



	public static function getChildren($table, $parent_key = null, $primary_key = null) {
		$node = get($table, $parent_key, $primary_key);
		return $node['children'];
	}
}
