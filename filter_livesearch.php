<?php
class XA_Filter_LiveSearch extends XA_Filter {

	public $value;
	public $field;
	public $display;
	public $source; // url zrodla danych


	public function __construct($name, $label, $field, $source, $value = null, $display = null) {
		parent::__construct($name, $label);
		$this->field = $field;
		$this->value = $value;
		$this->display = $display;
		$this->source = $source;
	}


	public function out() {
		return XA_XHTML_Form::getField(array(
			  'type' => 'livesearch'
			, 'name' => $this->name
			, 'source' => $this->source
			, 'label' => $this->label
			, 'value' => $this->value
			, 'display' => $this->display
		));
	}


	public function modifyQuery($query) {
		if ($this->value) {
			$query->conditions[$this->name.'_filter'] = $this->field." = ".$this->value;
		}
	}
}
