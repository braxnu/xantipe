<?php
namespace xa;

abstract class ListView {

	public $query;
	public $page_number = 1;
	public $items_per_page = 20;
	public $page_count = null;
	public $row_list = [];



	public function __construct ($query = null) {
		if (is_a($query, '\\xa\\db\\Query')) {
			$this->query = $query;
		} else {
			$this->row_list = $query;
		}
	}



	/**
	 * You can implement this function in child class if you want anything
	 * to be done with retrieved rows before they are displayed.
	 */
	public function prepare_rows () {
		return true;
	}



	public function fetch_rows () {
		if ($this->query) {
			$this->query->limit($this->items_per_page);


			if ($this->items_per_page) {
				$offset = ($this->page_number - 1) * $this->items_per_page;
			} else {
				// no pagination, display all on one page (this doesn't mean
				// "no limit")
				$offset = 0;
			}

 			$this->query->offset($offset);


			$count_query = clone $this->query;
			$count_query->select('COUNT(1)', 'count');

			$count_query->offset(0);
			$count_query->limit(null);
			$count_query->order_rule_list = [];

			$count_result = xa::query($count_query);

			if ($count_result->count() > 1) {
				// 'group by' was used
				$row_count = $count_result->count();
			} else {
				$row_count = $count_result->val();
			}

			if ($this->items_per_page) {
				$this->page_count = (int)ceil($row_count / $this->items_per_page);
			} else {
				$this->page_count = 1;
			}

			$this->row_list = xa::query($this->query)->all((isset($this->pk_list) && count($this->pk_list) === 1) ? $this->pk_list[0] : null);

			$this->prepare_rows();
		} elseif (!$this->page_count) {
			$this->page_count = 1;
		}
	}
}
