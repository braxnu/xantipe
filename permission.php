<?php
abstract class XA_Permission {

	public static $worker_list = array();
	
	
	
	abstract public function check($perm_name, $params = array());



	public static function can($perm_name, $params = array()) {
	
		if (empty(self::$worker_list[$perm_name])) {
			$file_name = MODULE_PATH.'permissions/'.$perm_name.'.php';

			if (file_exists($file_name)) {
				include($file_name);
				$class_name = self::get_class_name($perm_name);
			} else {
				$class_name = 'XA_Permission_Simple';
			}
			
			self::$worker_list[$perm_name] = new $class_name();
		}
		
		$worker = self::$worker_list[$perm_name];
		return $worker->check($perm_name, $params);
	}



	public static function get_class_name($permission_name) {
		$chunk_list = array('XA', 'Permission');
		
		foreach (explode('_', $permission_name) as $chunk) {
			$chunk_list[] = ucfirst($chunk);
		}
		
		return implode('_', $chunk_list);
	}
}
