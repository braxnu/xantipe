<?php
class XA_EditForm extends XA_XHTML_Form {

	public $record_exists;
	public $table;
	public $table_name;



	public function __construct($table_name, $locked_field_list = array()) {

		$this->addClass($table_name.'_edit');
		$this->table_name = $table_name;

		$this->alert_list = array(
			'empty' => 'Pole jest wymagane'
		);

		$this->addHidden('tmpdata_id', md5(mt_rand().time().$table_name));

		$this->table = xa::table($table_name);
		$primary_key_list = $this->table->getPrimaryKeyList();
		$field_list = $this->table->getFieldList();

		foreach ($locked_field_list as $field_name) {
			unset($field_list[$field_name]);
		}

		foreach ($field_list as $field_name => $field) {
			switch (true) {
				case (in_array($field_name, $primary_key_list)): {
					$this->addHidden($field_name);
					break;
				}

				case ($field['type'] === 'set'): {
					$this->addSet($field_name, $field['option_list']);
					break;
				}

				case ($field['type'] === 'enum'): {
					$this->addRadio($field_name, $field['option_list']);
					break;
				}

				case ($field['type'] === 'datetime'): {
					$this->addText($field_name, 16);
					$this->field_list[$field_name]['class'] = 'datetimepicker';

					if (empty($value)) {
						$this->field_list[$field_name]['value'] = date('Y-m-d H:i');
					}

					break;
				}

				case ($field['type'] === 'date'): {
					$this->addText($field_name, 10);
					$this->field_list[$field_name]['class'] = 'datepicker';
					break;
				}

				case ($field['type'] === 'time'): {
					$this->addText($field_name, 5);
					$this->field_list[$field_name]['class'] = 'timepicker';
					break;
				}

				case (substr_count($field['type'], 'text')): {
					$this->addTextArea($field_name);
					break;
				}

				case (substr_count($field['type'], 'char')): {
					$this->addText($field_name, $field['size']);
					break;
				}

				case (substr_count($field['type'], 'int')): {
					$this->addText($field_name, $field['size']);
					break;
				}

				case (substr_count($field['type'], 'float')): {
					$this->addText($field_name, $field['size']);
					break;
				}

				default: {
					$this->addText($field_name);

					if (!empty($field['size'])) {
						$this->field_list[$field_name]['size'] = $field['size'];
					}
				}
			}

			if (isset($this->label_list[$field_name])) {
				$this->field_list[$field_name]['label'] = $this->label_list[$field_name];
			}
		}

		$this->addSubmit('submit', xa::say('Zapisz'));
	}



	public function load($data = null) {
		if (is_null($data)) {
			$data = $_GET;
		}

		$tmpdata_id = xa_in::symbol($data['tmpdata_id']);

		if ($tmpdata_id) {
			if (empty($_SESSION[$this->table_name][$tmpdata_id])) {
				return false;
			}

			$this->field_list['tmpdata_id']['value'] = $tmpdata_id;
			$session =& $_SESSION[$this->table_name][$tmpdata_id];
			$this->error_list = $session['error_list'];
			$this->setValues($session['form_data']);

			if ($filter = $this->table->filter($session['form_data'], true)) {
				$this->record_exists = $this->table->exists($filter);
			}
		} elseif ($filter = $this->table->filter($data, true)) {
			if ($record = $this->table->find($filter)) {
				$this->setValues($record);
				$this->record_exists = true;
			}
		}

		return true;
	}
}
