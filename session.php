<?php

class XA_Session {

	public $session;


	public function __construct() {
		$this->session =& $_SESSION[get_class($this)];
	}



	public function clear() {
		$this->session = array();
	}



	public function __get($name) {
		return &$this->session[$name];
	}



	public function __set($name, $value) {
		$this->session[$name] = $value;
	}
}
