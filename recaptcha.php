<?php
namespace xa;

class Recaptcha {



    public static function is_valid (& $captcha_token) {
        if (empty($_POST['g-recaptcha-response'])) {
            return false;
        }

        $api_url = 'https://www.google.com/recaptcha/api/siteverify'.
            '?secret='.\RECAPTCHA_SECRET_KEY.
            '&response='.$captcha_token;

        $captcha_api_response = file_get_contents($api_url);

        if (!$captcha_api_response) {
            return false;
        }

        $captcha_parsed_response = json_decode($captcha_api_response, true);

        return !empty($captcha_parsed_response['success']);
    }
}
