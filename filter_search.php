<?php
class XA_Filter_Search extends XA_Filter {

	public $value;
	public $field;


	public function __construct($name, $label, $field, $value = '') {
		parent::__construct($name, $label);
		$this->field = $field;
		$this->value = $value;
	}


	public function out() {
		return $this->wrap('
			<label for="'.$this->name.'_search_filter">'.$this->label.'</label>
			<input type="text" name="'.$this->name.'" id="'.$this->name.'_search_filter" value="'.$this->value.'"/>
		');
	}


	public function modifyQuery($query) {
		if (strlen($this->value)) {
			if (is_array($this->field)) {
				$cond_list = array();
				foreach ($this->field as $field) {
					$cond_list[] = $field." LIKE '%".xa_db::esc($this->value)."%'";
				}
				$query->conditions[$this->name.'_filter'] = "(".implode(' OR ', $cond_list).")";
			} else {
				$query->conditions[$this->name.'_filter'] = $this->field." LIKE '%".xa_db::esc($this->value)."%'";
			}
		}
	}
}
