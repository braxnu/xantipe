<?php
namespace xa;

class File {

    private static $finfo_mime;



    /**
     * Returns absolute file path.
     *
     * @param {int} $id File id.
     * @returns string
     */
    public static function path ($id) {
        return FILE_REPOSITORY_PATH . self::sub($id);
    }



    /**
     * Returns mimetype of a file given by path.
     *
     * @returns string
     */
    public static function get_mimetype ($file_path) {
        if (empty(self::$finfo_mime)) {
            self::$finfo_mime = new \finfo(\FILEINFO_MIME_TYPE);
        }

        return self::$finfo_mime->file($file_path);
    }



    /**
     * Returns file path inside of the repository. Useful, when repository
     * structure needs to be resembled under another path.
     *
     * @param {int} $id File id.
     * @returns string
     */
    public static function sub ($id) {
        $file_padded_name = str_pad($id, 6, '0', STR_PAD_LEFT);

        return substr($file_padded_name, 0, 3).
               '/'.
               substr($file_padded_name, 3, 3);
    }



    /**
     * Updates contents of the file under given file id.
     *
     * @param {array} $file
     * @returns bool Success
     */
    public static function update ($file) {
        if (!empty($file['content'])) {
            $file['path'] = self::save_temp($file['content']);
        }

        if (!empty($file['path'])) {
            if (!is_readable($file['path'])) {
                throw new \Exception("Could not read file $path");
            }

            $file['type'] = self::get_mimetype($file['path']);

            $file_save_path = self::path($file['file_id']);
            $file_dir = dirname($file_save_path);

            if (!file_exists($file_dir)) {
                if (!mkdir($file_dir, 0777, true)) {
                    throw new \Exception('Could not create directory '.$file_dir);
                }
            }

            self::copy($file['path'], $file_save_path);
        }

        return \xa::table('file')->update($file);
    }



    /**
     * Saves given file content to a temporary file and returns its absolute
     * path.
     *
     * @param {string} $file_contents Raw contents of the file.
     * @returns string Path to a temporary file.
     */
    public static function save_temp ($file_contents) {
        $tmp_file_name = tempnam(sys_get_temp_dir(), \APP_ID);
        file_put_contents($tmp_file_name, $file_contents);

        return $tmp_file_name;
    }



    /**
     * Adds a new file to the file repository.
     *
     * @param {string} $path Path to the file.
     * @param {string} $name File name.
     * @returns int New file id.
     */
    public static function add ($path, $name) {
        if (!is_readable($path)) {
            throw new \Exception("Could not read file $path");
        }

        $file_id = \xa::table('file')->insert([
            'name' => $name,
            'type' => self::get_mimetype($path)
        ]);

        $file_save_path = self::path($file_id);
        $file_dir = dirname($file_save_path);

        if (!file_exists($file_dir)) {
            if (!mkdir($file_dir, 0777, true)) {
                throw new \Exception('Could not create directory '.$file_dir);
            }
        }

        self::copy($path, $file_save_path);
        chmod($file_save_path, 0666);

        return $file_id;
    }



    /**
     * Copies a file.
     *
     * @param {string} $src Path to the file.
     * @param {string} $dest Destination path.
     */
    public static function copy ($src, $dest) {
        $dest_dir = dirname($dest);

        if (!is_writable($dest_dir)) {
            if (!mkdir($dest_dir, 0777, true)) {
                throw new \Exception('Could not create directory '.$dest_dir);
            }
        }

        if (is_uploaded_file($src)) {
            if (!move_uploaded_file($src, $dest)) {
                throw new \Exception('Could not write file '.$dest);
            }
        } else {
            if (!copy($src, $dest)) {
                throw new \Exception('Could not write file '.$dest);
            }
        }
    }



    /**
     * Adds POST-uploaded file to the repository.
     *
     * @param {string} $field_name Form field name in the $_FILES array.
     * @returns int New file id.
     */
    public static function receive ($field_name) {
        if (empty($_FILES[$field_name])) {
            return false;
        } else {
            return self::add(
                $_FILES[$field_name]['tmp_name'],
                $_FILES[$field_name]['name']
            );
        }
    }



    /**
     * Removes file from the repository.
     *
     * @param {int} $file_id File id.
     */
    public static function remove ($id) {
        \xa::table('file')->delete($id);

        if (file_exists(self::path($id))) {
            unlink(self::path($id));
        }
    }



    /**
     * Gets file data from repository.
     *
     * @param {int} $file_id File id.
     * @returns array
     */
    public static function get ($id) {
        return
            \xa::table('file')->find($id) +
            ['path' => self::path($id)];
    }
}
