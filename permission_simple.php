<?php
class XA_Permission_Simple extends XA_Permission {



	public function check ($perm_name, $params = array()) {
		if (empty($GLOBALS['user'])) {
			return false;
		}

		$user_id = $GLOBALS['user']->id;

		$permission = xa::table('permission')->find(array('name' => $perm_name));

		if (!$permission) {
			if (
				defined('CREATE_PERMISSION_IF_NOT_EXISTS')
				&&
				CREATE_PERMISSION_IF_NOT_EXISTS
			) {
				xa::table('permission')->insert(array(
					  'name' => $perm_name
					, 'description' => $perm_name
				));
			}

			return true;
		}

		$user_permission = xa::table('user_permission')->find(array(
			  'user_id' => $user_id
			, 'permission_id' => $permission['permission_id']
		));

		// DELME
		if (!$user_permission) {
			xa::table('user_permission')->insert(array(
				  'user_id' => $user_id
				, 'permission_id' => $permission['permission_id']
				, 'allow' => 1
			));

			return true;
		}

		return $user_permission['allow'];
	}
}
