<?php
namespace xa;

class Trace {



    /**
     * Writes log entry to one or more log files.
     *
     * @param {array|string} $params Entry properties. Special keys (flags) are:
     *    's' - adds session_id to entry
     *    'u' - adds user_id to entry
     * @param {array|string} $log_list List of log file names (without extensions).
     *    If string given, it's used ad one element array.
     * @example \xa\Log::log(
     * @example     ['action' => 'insert', 'index' => 23],
     * @example     ['db-opeartions']
     * @example );
     */
    public static function trace ($params = [], $log_list) {
        if (is_string($log_list)) {
            $log_list = [$log_list];
        }

        if (!is_array($params)) {
            $params = [$params];
        }

        $head_line = [
            date('Y-m-d H:i:s')
        ];

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $head_line[] = $_SERVER['REMOTE_ADDR'];
        }

        if (!empty($params['_s']) && ($sid = session_id())) {
            $head_line[] = 'session_id:'.$sid;
            unset($params['_s']);
        }

        if (!empty($params['_u'])) {
            if (\xa\User::id()) {
                $head_line[] = 'user_id:'.\xa\User::id();
            }

            unset($params['_u']);
        }


        $content_lines = [];

        foreach ($params as $key => $val) {
            $content_lines[] = ($key ? $key.':' : '').$val."\n";
        }

        $message =
            implode(' ', $head_line)."\n".
            implode('', $content_lines)."\n";

        foreach ($log_list as $log) {
            $file_path = \LOG_PATH.$log.'.log';
            $dir_path = dirname($file_path);

            if (!is_dir($dir_path)) {
                if (!mkdir(dirname($file_path), 0777)) {
                    throw new \Exception("Could not write $file_path");
                }
            }

            $file = fopen($file_path, 'a');

            if (!$file) {
                throw new \Exception("Could not append to $file_path");
            }

            fwrite($file, $message);
            fclose($file);
        }
    }



    public static function getEntry ($line) {
        $chunks = explode(' ', trim($line));

        $record = array(
            'date' => array_shift($chunks).' '.array_shift($chunks),
            'ip' => array_shift($chunks)
        );

        foreach ($chunks as $item) {
            list($key, $val) = explode(':', $item);

            if ($key === 's') {
                $key = 'session_id';
            }

            if ($key === 'u') {
                $key = 'user_id';
            }

            $record[$key] = $val;
        }

        return $record;
    }
}
