<?php
namespace xa;

abstract class Timepiece {



    public static function get($then, $now = null) {
        $now = ($now ? strtotime($now) : time());
        $then = strtotime($then);

        if (date('Ymd', $now) === date('Ymd', $then)) {
            // same day
            $diff_h = (int)floor(($now - $then) / 3600);

            switch (true) {
                case ($diff_h >= 22):
                    return $diff_h.' godziny temu';
                case ($diff_h >= 5):
                    return $diff_h.' godzin temu';
                case ($diff_h >= 2):
                    return $diff_h.' godziny temu';
                case ($diff_h === 1):
                    return 'godzinę temu';
                case ($diff_h === 0):
                    $diff_m = intval(floor(($now - $then) / 60));

                    if ($diff_m > 2) {
                        $digits = str_split(strval($diff_m));
                        $last_digit = intval(end($digits));

                        if ($last_digit < 2 || $last_digit > 4 || ($diff_m < 20 && $diff_m > 10)) {
                            return $diff_m.' minut temu';
                        } else {
                            return $diff_m.' minuty temu';
                        }
                    } else {
                            return 'przed chwilą';
                    }
            }
        } elseif (date('Ymd', $now - 24 * 3600) === date('Ymd', $then)) {
            return 'wczoraj, '.date('H:i', $then);
        } elseif (floor(($now - $then) / 3600 / 24) < 7) {
            // return mb_convert_case(iconv("Windows-1250", "UTF-8", strftime('%A, %H:%M', $then)), MB_CASE_TITLE, "UTF-8");
            return mb_convert_case(strftime('%A, %H:%M', $then), MB_CASE_TITLE, "UTF-8");

        } else {
            // return mb_convert_case(iconv("Windows-1250", "UTF-8", strftime('%d %b %Y, %a', $then)), MB_CASE_TITLE, "UTF-8");
            return mb_convert_case(strftime('%d %b %Y, %a', $then), MB_CASE_TITLE, "UTF-8");
        }
    }
}
