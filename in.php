<?php
namespace xa;

class In {



    public static function nat (&$number) {
        return self::get_nat($number);
    }



    public static function is_nat (&$number) {
        return
            !empty($number)
            &&
            is_scalar($number)
            &&
            strval($number) === strval(abs(intval($number)));
    }



    public static function get_nat (&$number) {
        if (
            !empty($number)
            &&
            is_scalar($number)
            &&
            strval($number) === strval(abs(intval($number)))
        ) {
            return intval($number);
        }

        return false;
    }



    public static function is_int (&$value) {
        if (!empty($value) && is_array($value)) {
            return false;
        }

        return isset($value) && preg_match('/^(\-?\d+)$/', $value);
    }



    public static function get_int (&$number) {
        if (isset($number) && preg_match('/^(\-?\d+)$/', $number)) {
            return intval($number);
        }

        return false;
    }



    public static function is_float (&$number) {
        if (!isset($number)) {
            return false;
        }

        $number = strtr($number, ',', '.');

        return preg_match('/^\-?(\d+)|(\d*\.\d+)$/', $number);
    }



    public static function get_float (&$number) {
        if (!isset($number)) {
            return 0;
        }

        $number = strtr($number, ',', '.');

        if (preg_match('/^\-?(\d+)|(\d*\.\d+)$/', $number)) {
            return floatval($number);
        } else {
            return 0;
        }
    }



    public static function is_date (&$input) {
        return isset($input) && strtotime($input) !== false;
    }



    public static function get_date (&$input) {
        if (isset($input)) {
            if (($time = strtotime($input)) !== false) {
                return date('Y-m-d', $time);
            }
        }

        return false;
    }



    public static function datetime (&$input) {
        if (isset($input)) {
            if (($time = strtotime($input)) !== false) {
                return date('Y-m-d H:i:s', $time);
            }
        }

        return false;
    }



    public static function time (&$input) {
        if (isset($input)) {
            if (($time = strtotime($input)) !== false) {
                return date('H:i', $time);
            }
        }

        return false;
    }



    public static function is_symbol (&$input) {
        return isset($input) && preg_match('/^[0-9a-z\_\-]+$/i', $input);
    }



    public static function get_symbol (&$input) {
        if (isset($input) && preg_match('/^[0-9a-z\_\-]+$/i', $input)) {
            return $input;
        }

        return false;
    }



    public static function is_email (&$value) {
        return isset($value) && filter_var($value, FILTER_VALIDATE_EMAIL);
    }



    public static function in_dict (&$value, $dict) {
        return isset($value) && in_array($value, $dict);
    }



    public static function dict (&$input, $value_list) {
        if (isset($input) && in_array($input, $value_list)) {
            return $input;
        }

        return false;
    }



    public static function set (&$input) {
        return isset($input) ? explode(',', $input) : [];
    }



    public static function clean (&$input) {
        return isset($input) ? trim(strip_tags($input)) : '';
    }



    public static function sanitize (&$input) {
        return isset($input) ? trim(strip_tags($input)) : '';
    }
}
