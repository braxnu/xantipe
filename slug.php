<?php
namespace xa;

class Slug {



	public static function get ($text, $length = 1024, $delimiter = '-') {
		$text = strtr(
            mb_strtolower($text, 'UTF-8'),
            [
                'ą' => 'a',
                'ć' => 'c',
                'ę' => 'e',
                'ł' => 'l',
                'ń' => 'n',
                'ó' => 'o',
                'ś' => 's',
                'ź' => 'z',
                'ż' => 'z'
            ]
        );

		$text = preg_replace('/[^0-9a-z]/', ' ', $text);
		$count = 0;

		do {
			$text = str_replace('  ',  ' ',  $text, $count);
		} while ($count);

		$text = str_replace(' ', $delimiter, trim(substr($text, 0, $length)), $count);

		return $text;
	}



    public static function increment ($slug, $max_length) {
        $match = [];
        preg_match('/(\d*)$/', $slug, $match);
        $ending_number = $match[1];
        $slug = substr($slug, 0, strlen($slug) - strlen($ending_number));

        if (!$ending_number) {
            $ending_number = 1;
        }

        $ending_number++;
        $length_to_be_cut = strlen($slug.$ending_number) - $max_length;

        if ($length_to_be_cut < 0) {
            $length_to_be_cut = 0;
        }

        $cut_slug = substr($slug, 0, strlen($slug) - $length_to_be_cut);

        return $cut_slug.$ending_number;
    }
}
