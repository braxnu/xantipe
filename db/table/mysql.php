<?php
namespace xa\db\table;

class MySQL extends \xa\db\Table {



    public function __construct($table_name, $db_instance_name = 'default') {
        parent::__construct($table_name, $db_instance_name);

        $sql = "DESCRIBE `$table_name`";

        foreach ($this->db->query($sql)->all('Field') as $field_name => $field_desc) {

            if (substr_count($field_desc['Key'], 'PRI')) {
                $this->pk_name_list[] = $field_name;
            }

            if ($field_desc['Extra'] === 'auto_increment') {
                $this->sequence_field_name = $field_name;
            }

            $field = [];

            $field['null'] = ($field_desc['Null'] === 'YES');

            if (!empty($field_desc['Default']) || ($field_desc['Default'] === '0')) {
                $field['default'] = $field_desc['Default'];
            }

            $field['unsigned'] = (bool)substr_count($field_desc['Type'], 'unsigned');

            switch (true) {
                case (substr_count($field_desc['Type'], 'char')): {
                    $field['type'] = 'char';
                    $field['size'] = intval(substr($field_desc['Type'], stripos($field_desc['Type'], '(') + 1, -1));
                    break;
                }
                case (substr_count($field_desc['Type'], 'int')): {
                    $field['type'] = 'int';
                    $field['size'] = intval(substr($field_desc['Type'], stripos($field_desc['Type'], '(') + 1, -1));
                    break;
                }
                case ($field_desc['Type'] === 'float'): {
                    $field['size'] = 20;
                    $field['type'] = 'float';
                    break;
                }
                case ($field_desc['Type'] === 'datetime'): {
                    $field['type'] = 'datetime';
                    break;
                }
                case ($field_desc['Type'] === 'date'): {
                    $field['type'] = 'date';
                    break;
                }
                case ($field_desc['Type'] === 'time'): {
                    $field['type'] = 'time';
                    break;
                }
                case ($field_desc['Type'] === 'tinytext'): {
                    $field['type'] = 'tinytext';
                    break;
                }
                case (substr_count($field_desc['Type'], 'text')): {
                    $field['type'] = 'text';
                    break;
                }
                case (stripos($field_desc['Type'], 'decimal') === 0): {
                    $field['type'] = 'float';
                    list($unit_size, $fraction_size) = explode(',', substr($field_desc['Type'], stripos($field_desc['Type'], '(') + 1, - 1));
                    $field['size'] = $unit_size + $fraction_size;
                    break;
                }
                case (stripos($field_desc['Type'], 'set') === 0): {
                    $option_list = array();
                    $field['type'] = 'set';
                    foreach (explode(',', trim(substr($field_desc['Type'], 3), '()')) as $option_name) {
                        $option_name = trim($option_name, '\'');
                        $option_list[$option_name] = $option_name;
                    }
                    $field['option_list'] = $option_list;
                    break;
                }
                case (stripos($field_desc['Type'], 'enum') === 0): {
                    $option_list = array();
                    $field['type'] = 'enum';
                    foreach (explode(',', trim(substr($field_desc['Type'], 4), '()')) as $option_name) {
                        $option_name = trim($option_name, '\'');
                        $option_list[$option_name] = $option_name;
                    }
                    $field['option_list'] = $option_list;
                    break;
                }

                default: {
                    throw new Error('Unsupported column type '.print_r($field_desc, true));
                }
            }

            $this->field_list[$field_name] = $field;
        }
    }



    public function prepare_field ($field_name) {
        return '`'.$field_name.'`';
    }



    public function prepare_value ($value, $field_name) {
        $field = $this->field_list[$field_name];

        if ($field['null'] && ($value === '' || !isset($value))) {
            return 'NULL';
        }

        if ($field['type'] === 'int') {
            return intval($value);
        } elseif ($field['type'] === 'float') {
            if (!\xa\In::is_float($value)) {
                throw new \Exception('Value '.$value.' of '.$field_name.' is not float');
            }

            return \xa\in::get_float($value);
        }

        // string
        if (
            $field['type'] === 'char'
            &&
            !empty($field['size'])
            &&
            $field['size'] < mb_strlen($value)
        ) {
            $value = mb_substr($value, 0, $field['size']);
        }

        return "'".$this->db->escape_string($value)."'";
    }
}
