<?php
namespace xa\db\table;

class PgSQL extends \xa\db\Table {



    public function __construct($table_name, $db_instance_name = 'default') {
        parent::__construct($table_name, $db_instance_name);

        $sql = "
            SELECT
                pg_attribute.attname
            FROM
                pg_index,
                pg_class,
                pg_attribute
            WHERE
                pg_class.oid = '$table_name'::regclass
                AND
                indrelid = pg_class.oid
                AND
                pg_attribute.attrelid = pg_class.oid
                AND
                pg_attribute.attnum = any(pg_index.indkey)
                AND
                indisprimary
        ";

        $this->pk_name_list = $this->db->query($sql)->col('attname');

        $sql = "
            SELECT
                *
            FROM
                information_schema.columns
            WHERE
                table_name = '$table_name'
            ORDER BY
                ordinal_position ASC
        ";

        foreach ($this->db->query($sql)->all('column_name') as $field_name => $field_desc) {
            $field = [
                  'unsigned' => false
                , 'default' => null
                , 'null' => $field_desc['is_nullable'] !== 'NO'
            ];

            if (stristr($field_desc['column_default'], 'nextval(') !== false) {
                $this->sequence_field_name = $field_name;
            } elseif (
                !empty($field_desc['column_default'])
                ||
                $field_desc['column_default'] === '0'
            ) {
                $field['default'] = $field_desc['column_default'];
            }

            switch (true) {
                case (
                    substr_count($field_desc['data_type'], 'integer')
                    ||
                    $field_desc['data_type'] === 'smallint'
                ): {
                    $field['type'] = 'int';

                    $field['size'] = $field_desc['numeric_precision_radix'] == 2
                        // TODO better size detection
                        ? $field_desc['numeric_precision'] / 4
                        : $field_desc['numeric_precision'];

                    break;
                }

                case (substr_count($field_desc['data_type'], 'char')): {
                    $field['type'] = 'char';
                    $field['size'] = $field_desc['character_maximum_length'];

                    break;
                }

                case ($field_desc['data_type'] === 'float'): {
                    // TODO size detection
                    $field['size'] = 20;
                    $field['type'] = 'float';
                    break;
                }

                case (substr_count($field_desc['data_type'], 'timestamp')): {
                    $field['type'] = 'datetime';
                    break;
                }

                case ($field_desc['data_type'] === 'date'): {
                    $field['type'] = 'date';
                    break;
                }

                case ($field_desc['data_type'] === 'time'): {
                    $field['type'] = 'time';
                    break;
                }

                case ($field_desc['data_type'] === 'tinytext'): {
                    $field['type'] = 'tinytext';
                    break;
                }

                case (substr_count($field_desc['data_type'], 'text')): {
                    $field['type'] = 'text';
                    break;
                }

                case ($field_desc['data_type'] === 'numeric'): {
                    // TODO size detection
                    $field['type'] = 'float';
                    //$unit_size = $field_desc['numeric_precision'] - $field_desc['numeric_scale'];
                    //$fraction_size = $field_desc['numeric_scale'];
                    $field['size'] = $field_desc['numeric_precision'];
                    break;
                }

                default: {
                    throw new Error('Unsupported column type '.print_r($field_desc, true));
                }
            }

            $this->field_list[$field_name] = $field;
        }
    }



    public function prepare_field ($field_name) {
        return '"'.$field_name.'"';
    }



    public function prepare_value ($value, $field_name) {
        $field = $this->field_list[$field_name];

        if ($field['null'] && (!isset($value) || is_null($value) || $value === '')) {
            return 'NULL';
        }

        if ($field['type'] === 'int') {
            return intval($value);
        } elseif ($field['type'] === 'float') {
            if (!\xa\In::is_float($value)) {
                throw new \Exception('Value '.$value.' of '.$field_name.' is not float');
            }

            return \xa\in::get_float($value);
        }

        return "'".$this->db->escape_string($value)."'";
    }
}
