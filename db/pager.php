<?php
namespace xa\db;

class Pager {

    public $page_number = 1;
    public $items_per_page = 20;
    protected $query;
    protected $row_list;
    protected $page_count;
    protected $has_fetched = false;
    protected $db;



    public function __construct (\xa\db\Query $query, \xa\DB $db) {
        $this->query = $query;
        $this->db = $db;
    }



    protected function fetch () {
        $this->query->limit($this->items_per_page);
        $this->query->offset(($this->page_number - 1) * $this->items_per_page);

        $this->row_list = $this->db->query($this->query)->all();

        $count_query = clone $this->query;

        $count_query->field_list = [];
        $count_query->select('COUNT(1)');
        $count_query->offset(0);
        $count_query->limit(null);
        $count_query->order_rule_list = [];

        $count_result = $this->db->query($count_query);

        if ($count_result->count() > 1) {
            // 'group by' was used
            $row_count = $count_result->count();
        } else {
            $row_count = $count_result->val();
        }

        $this->page_count = (int)ceil($row_count / $this->items_per_page);
        $this->has_fetched = true;
    }



    public function get_rows () {
        if (!$this->has_fetched) {
            $this->fetch();
        }

        return $this->row_list;
    }



    public function get_page_count () {
        if (!$this->has_fetched) {
            $this->fetch();
        }

        return $this->page_count;
    }
}
