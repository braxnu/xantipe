<?php
namespace xa\db;

class Query {

	public $table_list = [];
	public $field_list = [];
	public $condition_list = [];
	public $order_rule_list = [];
	public $group_rule_list = [];
	public $offset = 0;
	public $limit = null;
	public $distinct = false;



	public function __construct ($table_name, $alias = null) {
		$table = [
			'name' => $table_name
		];

		if ($alias) {
			$table['alias'] = $alias;
		}

		$this->table_list[] = $table;
	}



	public function select ($expression, $alias = null) {
		$field = ['expression' => $expression];

		if ($alias) {
			$field['alias'] = $alias;
			$index = $alias;
		} else {
			$expression_chunks = explode('.', $expression);
			$last_chunk = end($expression_chunks);

			if (\xa\In::is_symbol($last_chunk)) {
				$index = $last_chunk;
			}
		}

		if (
			!empty($index)
			&&
			!isset($this->field_list[$index])
		) {
			$this->field_list[$index] = $field;
		} else {
			$this->field_list[] = $field;
		}


		return $this;
	}



	public function where ($condition) {
		$this->condition_list[] = $condition;

		return $this;
	}



	public function inner ($table_name, $alias, $condition = null) {
		if (!$condition) {
			$condition = $alias;
			$alias = null;
		}

		$table = [
			  'name' => $table_name
			, 'condition' => $condition
			, 'join' => 'inner'
		];

		if ($alias) {
			$table['alias'] = $alias;
		}

		$this->table_list[] = $table;

		return $this;
	}



	public function left ($table_name, $alias, $condition = null) {
		if (!$condition) {
			$condition = $alias;
			$alias = null;
		}

		$table = [
			  'name' => $table_name
			, 'condition' => $condition
			, 'join' => 'left'
		];

		if ($alias) {
			$table['alias'] = $alias;
		}

		$this->table_list[] = $table;

		return $this;
	}



	public function order_by ($field, $order = 'ASC') {
		$this->order_rule_list[] = $field.' '.$order;

		return $this;
	}



	public function group_by ($field) {
		$this->group_rule_list[] = $field;

		return $this;
	}



	public function limit ($limit) {
		$this->limit = $limit;

		return $this;
	}



	public function offset ($offset) {
		$this->offset = $offset;

		return $this;
	}



	public function distinct ($bool) {
		$this->distinct = $bool;

		return $this;
	}
}
