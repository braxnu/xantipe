<?php
namespace xa\db\query_renderer;

abstract class MySQL {

    const QC = '`';



    public static function out (\xa\db\Query $query) {
        $qc = self::QC;
        $select_fields = [];

        foreach ($query->field_list as $field) {
            $sql = $field['expression'];

            if (isset($field['alias'])) {
                $sql .= ' AS '.$qc.$field['alias'].$qc;
            }

            $select_fields[] = $sql;
        }

        if (!$select_fields) {
            $select_fields = ['*'];
        }


        $table_list = [];

        foreach ($query->table_list as $table) {
            $sql = '';

            if (isset($table['join'])) {
                $sql .= ($table['join'] === 'left' ? 'LEFT ' : '').'JOIN ';
            }

            $sql .= $qc.$table['name'].$qc;

            if (isset($table['alias'])) {
                $sql .= ' AS '.$qc.$table['alias'].$qc;
            }

            if (isset($table['condition'])) {
                $sql .= ' ON '.$table['condition'];
            }

            $table_list[] = $sql;
        }


        return '
SELECT '.($query->distinct ? 'DISTINCT' : '').'
      '.implode('
    , ', $select_fields)."
FROM
    ".implode('
    ', $table_list)
    .($query->condition_list ? '
WHERE
    '.implode("
    AND
    ", $query->condition_list) : '')
    .($query->order_rule_list ? '
ORDER BY
      '.implode('
    , ', $query->order_rule_list) : '')
    .($query->group_rule_list ? '
GROUP BY
      '.implode(', ', $query->group_rule_list) : '')
    .(isset($query->limit) ? '
LIMIT
    '.$query->limit : '')
    .(!empty($query->offset) ? '
OFFSET
    '.$query->offset : '');
    }
}
