<?php
namespace xa\Db;

abstract class Manager {

  protected static $instance_dict = array();



  public static function get ($db_name = null) {
    if (!$db_name) {
      $db_name = key(Config::$db);
      var_dump($db_name);
    }

    if (empty(self::$instance_dict[$db_name])) {
      $config = Config::$db[$db_name];
      $class_name = '\\xa\\Db\\Driver\\'.$config['type'];
      self::$instance_dict[$db_name] = new $class_name($config);
    }

    return self::$instance_dict[$db_name];
  }
}
