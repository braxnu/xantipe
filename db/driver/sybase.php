<?php
class XA_Db_Sybase extends XA_Db {

   public $encoding = 'cp1250';



   public function __construct($host, $user, $password, $db_name) {
      $this->connection = sybase_connect($host, $user, $password, $this->encoding);
      sybase_select_db($db_name, $this->connection);
   }



   public function escapeString($string) {
      return addslashes($string);
   }



   public function query($query) {
      unset($this->insert_id);

      if (is_a($query, 'XA_Query')) {
         $query = $query->get();
      }

      if (is_resource($result = sybase_query($query, $this->connection))) {
         return new XA_Data_Sybase($result);
      } elseif ($result) {
         $insert_id_row = sybase_fetch_row(sybase_query("SELECT @@identity", $this->connection));
         return ($this->insert_id = $insert_id_row[0] ?: true);
      } else {
         if (!headers_sent()) {
            header('Content-Type: text/plain');
         }

         $exception = new Exception(sybase_get_last_message()."\r\n\r\n".$query."\r\n\r\n");
         trigger_error("\n".$exception);
         die($exception);
      }
   }



   public function begin_trans() {
      $this->query('BEGIN TRANSACTION');
   }



   public function end_trans($commit = false) {
      $this->query(($commit ? 'COMMIT' : 'ROLLBACK').' TRANSACTION');
   }



   public function getInsertId() {
      return $this->insert_id;
   }
}
