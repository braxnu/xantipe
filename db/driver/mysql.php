<?php
namespace xa\db\driver;

use \xa\db\query_renderer\MySQL as Renderer;

class MySQL extends \xa\DB {



    public function __construct ($config) {
        $this->type = $config['type'];

        $this->connection = new \mysqli(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['name']
        );

        if ($this->connection->connect_error) {
            throw new \Exception($this->connection->connect_error);
        }

        $this->connection->set_charset('utf8');
    }



    public function escape_string ($string) {
        if (!is_string($string) && !is_int($string)) {
            throw new \Exception('Expecting a string parameter');
        }

        return $this->connection->real_escape_string($string);
    }



    public function query ($query) {
        unset($this->insert_id);

        if ($query instanceof \xa\db\Query) {
            $query = Renderer::out($query);
        }

        $result = $this->connection->query($query);

        if (is_object($result)) {
            return new \xa\db\data\MySQL($result);
        } elseif ($result) {
            $this->insert_id = $this->connection->insert_id;

            return $this->insert_id ?: true;
        } else {
            throw new \Exception($this->connection->error."\n\n".$query);
        }
    }



    public function begin_trans () {
        $this->connection->query('START TRANSACTION');
    }



    public function end_trans ($commit = false) {
        $this->connection->query($commit ? 'COMMIT' : 'ROLLBACK');
    }
}
