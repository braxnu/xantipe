<?php
namespace xa\db\driver;

use \xa\db\query_renderer\PgSQL as Renderer;

class PgSQL extends \xa\DB {



    public function __construct ($config) {
        $this->type = $config['type'];

        $conn_string = 'host='.$config['host']
            . ' dbname='.$config['name']
            . ' user='.$config['user']
            . ' password='.$config['password'];

        if (!empty($config['port'])) {
            $conn_string .= ' port='.$config['port'];
        }

        $this->connection = pg_connect($conn_string);

        if (!$this->connection) {
            throw new \Exception(
                'Could not connect to PGSQL server.'
                .' host='.$config['host']
                .' dbname='.$config['name']
                .' user='.$config['user']
            );
        }
    }



    public function escape_string ($string) {
        if (!is_string($string) && !is_int($string)) {
            throw new \Exception('Expecting a string parameter');
        }

        return pg_escape_string($this->connection, $string);
    }



    public function query ($query, $return_new_id = false) {
        unset($this->insert_id);

        if ($query instanceof \xa\db\Query) {
            $query = Renderer::out($query);
        }

        $result = @pg_query($this->connection, $query);

        if ($result === false) {
            throw new \Exception(
                  pg_last_error($this->connection)
                . "\n\n".$query
            );
        }

        if (pg_result_status($result) === PGSQL_COMMAND_OK) {
            if (!$return_new_id) {
                return true;
            }

            $id_resource = pg_query($this->connection, 'SELECT lastval()');
            $row = pg_fetch_row($id_resource);

            return $row[0];
        }

        return new \xa\db\data\PgSQL($result);
    }



    protected function begin_trans () {
        $result = pg_query($this->connection, 'BEGIN');

        if ($result === false) {
            throw new \Exception(
                "Could not start transaction\n\n".
                pg_last_error($this->connection)
            );
        }
    }



    protected function end_trans ($commit = false) {
        $result = pg_query($this->connection, $commit ? 'COMMIT' : 'ROLLBACK');

        if ($result === false) {
            throw new \Exception(
                "Could not finish transaction\n\n".
                pg_last_error($this->connection)
            );
        }
    }
}
