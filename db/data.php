<?php
namespace xa\db;

abstract class Data {

	protected $result_resource;



	abstract public function reset();
	abstract public function count();
	abstract public function val($field_name);
	abstract public function row();
	abstract public function col($field_name, $index_by = null);
	abstract public function all($index_by = null);
}
