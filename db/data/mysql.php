<?php
namespace xa\db\data;

class MySQL extends \xa\db\Data {



    public function __construct ($result_resource) {
        $this->result_resource = $result_resource;
    }



    public function val ($column = null) {
        if ($column) {
            $row = $this->result_resource->fetch_assoc();

            return $row[$column];
        } else {
            $row = $this->result_resource->fetch_row();

            return $row[0];
        }
    }



    public function row () {
        return $this->result_resource->fetch_assoc();
    }



    public function col ($column = null, $index_by = null) {
        $return = [];

        if ($index_by) {
            while ($row = $this->result_resource->fetch_assoc()) {
                $return[$row[$index_by]] = $row[$column];
            }
        } elseif ($column) {
            while ($row = $this->result_resource->fetch_assoc()) {
                $return[] = $row[$column];
            }
        } else {
            while ($row = $this->result_resource->fetch_row()) {
                $return[] = $row[0];
            }
        }

        return $return;
    }



    public function all ($index_by = null) {
        $return = [];

        if ($index_by) {
            while ($row = $this->result_resource->fetch_assoc()) {
                $return[$row[$index_by]] = $row;
            }
        } else {
            while ($row = $this->result_resource->fetch_assoc()) {
                $return[] = $row;
            }
        }

        return $return;
    }



    public function reset () {
        $this->result_resource->data_seek(0);
    }



    public function count () {
        return $this->result_resource->num_rows;
    }
}
