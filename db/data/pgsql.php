<?php
namespace xa\db\data;

class PgSQL extends \xa\db\Data {



	public function __construct ($result_resource) {
		$this->result_resource = $result_resource;
	}



	public function val ($column = null) {
		if ($column) {
			return pg_fetch_result($this->result_resource, $column);
		} else {
			$row = pg_fetch_row($this->result_resource);
			return $row ? current($row) : null;
		}
	}



	public function row () {
		return pg_fetch_assoc($this->result_resource);
	}



	public function col ($column = null, $index_by = null) {
		$return = [];

		if ($index_by) {
	    	while ($row = pg_fetch_assoc($this->result_resource)) {
				$return[$row[$index_by]] = $row[$column];
		    }
		} elseif ($column) {
		    while ($row = pg_fetch_assoc($this->result_resource)) {
                $return[] = $row[$column];
			}
		} else {
		    while ($row = pg_fetch_row($this->result_resource)) {
                $return[] = $row[0];
			}
        }

		return $return;
	}



	public function all ($index_by = null) {
		$return = [];

        if ($index_by) {
            while ($row = pg_fetch_assoc($this->result_resource)) {
                $return[$row[$index_by]] = $row;
            }
        } else {
            while ($row = pg_fetch_assoc($this->result_resource)) {
                $return[] = $row;
            }
        }

		return $return;
	}



	public function reset () {
		pg_result_seek($this->result_resource, 0);
	}



	public function count () {
		return pg_num_rows($this->result_resource);
	}
}
