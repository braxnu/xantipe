<?php
class XA_Data_Sybase extends XA_Data {


   public function __construct($result_resource) {
      $this->result_resource = $result_resource;
   }



   public function getRow() {
      return sybase_fetch_assoc($this->result_resource);
   }



   public function getRows($index = null) {
      $return = array();
      if (sybase_num_rows($this->result_resource)) {
         sybase_data_seek($this->result_resource, 0);
         while ($row = sybase_fetch_assoc($this->result_resource)) {
            if ($index) {
               $return[$row[$index]] = $row;
            } else {
               $return[] = $row;
            }
         }
      }
      return $return;
   }



   public function getOptionList($key_name, $value_name) {
      $return = array();
      if (sybase_num_rows($this->result_resource)) {
         sybase_data_seek($this->result_resource, 0);
         while ($row = sybase_fetch_assoc($this->result_resource)) {
            $return[$row[$key_name]] = $row[$value_name];
         }
      }
      return $return;
   }



   public function reset() {
      sybase_data_seek($this->result_resource, 0);
   }



   public function getRowCount() {
      return sybase_num_rows($this->result_resource);
   }



   public function count() {
      return sybase_num_rows($this->result_resource);
   }



   public function getValue($key = null) {
      if ($key) {
         return ($row = sybase_fetch_assoc($this->result_resource)) ? $row[$key] : null;
      } else {
         return ($row = sybase_fetch_row($this->result_resource)) ? current($row) : null;
      }
   }



   public function getValues($key = null, $index = null) {
      $return = array();
      while ($row = sybase_fetch_assoc($this->result_resource)) {
         if (!$key) {
            $key = key($row);
         }

         if ($index) {
            $return[$row[$index]] = $row[$key];
         } else {
            $return[] = $row[$key];
         }
      }
      return $return;
   }
}
