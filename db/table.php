<?php
namespace xa\db;

abstract class Table {

    public $name;
    protected $db;
    public $field_list = [];
    public $pk_name_list = [];
    public $sequence_field_name;



    abstract public function prepare_field ($field_name);
    abstract public function prepare_value ($value, $field_name);



    public function __construct ($table_name, $db_instance = null) {
        $this->name = $table_name;

        if (is_object($db_instance)) {
            $this->db = $db_instance;
        } else {
            // instance name passed
            $this->db = \xa\DB::get($db_instance);
        }
    }



    public function filter ($row, $pk_only = false) {
        $field_name_list =
            $pk_only
            ? $this->pk_name_list
            : array_keys($this->field_list);

        foreach (array_keys($row) as $field) {
            if (!in_array($field, $field_name_list)) {
                unset($row[$field]);
            }
        }

        return $row;
    }



    public function save ($row) {
        $primary_keys_only = $this->filter($row, true);

        if ($primary_keys_only && $this->exists($primary_keys_only)) {
            $this->update($row);

            return current($primary_keys_only);
        } else {
            return $this->insert($row);
        }
    }



    public function search ($field_list = []) {
        $query = new \xa\db\Query($this->name);

        foreach ($field_list as $field_name => $value) {
            $query->where(
                  $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($value, $field_name)
            );
        }

        return $this->db->query($query)->all();
    }



    public function find ($field_list, $pk_only = false) {
        if (
            !is_array($field_list)
            &&
            count($this->pk_name_list) === 1
        ) {
            $field_list = [$this->pk_name_list[0] => $field_list];
        }

        $field_list = $this->filter($field_list, $pk_only);

        $query = new \xa\db\Query($this->name);
        $query->limit(1);

        foreach ($field_list as $field_name => $value) {
            $query->where(
                  $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($value, $field_name)
            );
        }

        return $this->db->query($query)->row();
    }



    /**
     *
     * @returns bool
     */
    public function exists ($field_list, $pk_only = false) {
        if (
            !is_array($field_list)
            &&
            count($this->pk_name_list) === 1
        ) {
            $field_list = [$this->pk_name_list[0] => $field_list];
        }

        $field_list = $this->filter($field_list, $pk_only);

        $query = new \xa\db\Query($this->name);
        $query->select(1);
        $query->limit(1);

        foreach ($field_list as $field_name => $value) {
            $query->where(
                  $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($value, $field_name)
            );
        }

        return (bool)$this->db->query($query)->row();
    }



    public function count ($field_list) {
        $query = new \xa\db\Query($this->name);
        $query->select('COUNT(1)', 'count');

        foreach ($field_list as $field_name => $value) {
            $query->where(
                  $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($value, $field_name)
            );
        }

        return intval($this->db->query($query)->val());
    }



    public function insert ($row, $force = false) {
        $row = $this->filter($row);

        if ($this->sequence_field_name) {
            if (empty($row[$this->sequence_field_name])) {
                unset($row[$this->sequence_field_name]);
            } elseif (!$force) {
                throw new \Exception(
                    'Attempt to manually set PK value in sequential PK table '
                    .$this->name.' '
                    .'('.$this->sequence_field_name.')'
                );
            }
        } else {
            foreach ($this->pk_name_list as $pk_name) {
                if (empty($row[$pk_name])) {
                    throw new \Exception('PK value not set (' . $pk_name . ')');
                }
            }
        }

        $field_list = [];
        $value_list = [];

        foreach ($row as $field_name => $value) {
            $field_list[] = $this->prepare_field($field_name);
            $value_list[] = $this->prepare_value($value, $field_name);
        }

        // because "SELECT lastval()" causes error inside transaction in pgsql
        $return_new_id = !!$this->sequence_field_name;

        return $this->db->query(
            'INSERT INTO '
            . $this->prepare_field($this->name) . ' ('
            . implode(', ', $field_list)
            . ') VALUES ('
            . implode(', ', $value_list)
            . ')',
            $return_new_id
        );
    }



    public function update ($row) {
        $row = $this->filter($row);

        $condition_list = [];
        $value_list = [];

        foreach ($this->pk_name_list as $pk_name) {
            if (empty($row[$pk_name])) {
                throw new \Exception('PK value not set (' . $pk_name . ')');
            }

            $condition_list[] =
                $this->prepare_field($pk_name)
                . ' = '
                . $this->prepare_value($row[$pk_name], $pk_name);

            unset($row[$pk_name]);
        }

        foreach ($row as $field_name => $value) {
            $value_list[] =
                $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($value, $field_name);
        }

        return $this->db->query(
            'UPDATE '
            . $this->prepare_field($this->name)
            . ' SET '
            . implode(', ', $value_list)
            . ' WHERE '
            . implode(' AND ', $condition_list)
        );
    }



    public function delete ($row) {
        if (!is_array($row) && count($this->pk_name_list) === 1) {
            $row = [$this->pk_name_list[0] => $row];
        }

        $row = $this->filter($row);


        $condition_list = [];

        foreach ($row as $field_name => $val) {
            $condition_list[] =
                $this->prepare_field($field_name)
                . ' = '
                . $this->prepare_value($val, $field_name);
        }

        return $this->db->query(
            'DELETE FROM ' . $this->prepare_field($this->name)
            . ' WHERE ' . implode(' AND ', $condition_list));
    }



    public function get_primary_key_list () {
        return $this->pk_name_list;
    }



    public function get_field_list () {
        return $this->field_list;
    }
}
