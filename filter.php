<?php
abstract class XA_Filter {

	public $label;
	public $name;
	public $class_list;


	public function __construct($name, $label) {
		$this->name = $name;
		$this->label = $label;
		$this->class_list = array($name, 'filter');
	}


	public function wrap($body, $tag = 'p') {
		$script = '
		<script>
			$(document).ready(function() {
				prepareFilterContainer("'.$this->name.'", "'.$this->label.'");
			});
		</script>
		';
		
		return !$tag ? $body."\n".$script : '
<'.$tag.' class="'.implode(' ', $this->class_list).'">
	'.$body.'
</'.$tag.'>
		'.$script.'
		';
	}


	abstract public function modifyQuery($query);

	abstract public function out();
}
