<?php
namespace xa;

class Lang {

    public static $active_language_id;
    public static $cache = [];



    public static function detect_language_id ($accept_lang_header) {
        $accept_lang = strtolower($accept_lang_header);
        $accept_lang = str_replace('-', '_', $accept_lang);
        $chunks = [];

        foreach (explode(';', $accept_lang) as $bit) {
            foreach (explode(',', $bit) as $chunk) {
                $chunks[] = $chunk;
            }
        }

        $language_query = new \xa\db\Query('language');

        $language_id_map =
            \xa::query($language_query)->col('language_id', 'name');

        foreach ($chunks as $lang_symbol) {
            if (isset($language_id_map[$lang_symbol])) {
                return (int)$language_id_map[$lang_symbol];
            }
        }
    }



    public static function get_cache_file_path ($language_id) {
        return \FILE_REPOSITORY_PATH.'lang_cache_'.$language_id.'.ser';
    }



    public static function load_cache ($language_id) {
        $file_path = self::get_cache_file_path($language_id);

        if (file_exists($file_path)) {
            $cache = unserialize(file_get_contents($file_path));
        }

        self::$cache[$language_id] = empty($cache) ? [] : $cache;
    }



    public static function add_translation ($language_id, $text, $translation = null) {
        $cache = json_decode(
            file_get_contents(\APP_PATH.'translations.json'),
            true
        );

        $chars_to_escape = "\n\t'";
        $escaped_text = addcslashes($text, $chars_to_escape);

        $escaped_translation = isset($translation)
            ? addcslashes($translation, $chars_to_escape)
            : $escaped_text;

        if (!isset($cache[$escaped_text][$language_id])) {
            $cache[$escaped_text][$language_id] = $escaped_translation;

            file_put_contents(
                \APP_PATH.'translations.json',
                json_encode(
                    $cache,
                    \JSON_UNESCAPED_UNICODE | \JSON_PRETTY_PRINT
                )
            );
        }
    }



    public static function say ($text, $replacements = null, $language_id = null) {
        if (defined('NO_UI_TRANSLATIONS') && \NO_UI_TRANSLATIONS) {
            return $text;
        }

        if (!$language_id) {
            $language_id = \xa::$lang_id;
        }

        if (isset(self::$cache[$language_id][$text])) {
            $translation = self::$cache[$language_id][$text];
        } else {
            self::add_translation($language_id, $text);

            $translation = $text;
        }

        if (is_array($replacements)) {
            $counter = 1;

            foreach ($replacements as $subst) {
                $translation = str_replace(
                    '%'.$counter,
                    $subst,
                    $translation
                );

                $counter++;
            }
        }

        return $translation;
    }



    public static function update_cache () {
        $cache = json_decode(
            file_get_contents(\APP_PATH.'translations.json'),
            true
        );

        if (is_null($cache)) {
            throw new \Exception('Could not read translations.json');
        }

        $language_id_sql = new \xa\db\Query('language');
        $language_id_list = \xa::query($language_id_sql)->col('language_id');

        foreach ($language_id_list as $language_id) {
            $single_lang_cache = [];

            foreach ($cache as $text => $translations) {
                if (isset($translations[$language_id])) {
                    $single_lang_cache[$text] = $translations[$language_id];
                }
            }

            $file_path = self::get_cache_file_path($language_id);
            file_put_contents($file_path, serialize($single_lang_cache));
        }
    }
}
