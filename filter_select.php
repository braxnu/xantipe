<?php
class XA_Filter_Select extends XA_Filter {

	public $option_list = array();
	public $value;
	public $field;


	public function __construct($name, $label, $field, $option_list, $value = null) {
		parent::__construct($name, $label);
		$this->option_list = $option_list;
		$this->field = $field;
		$this->value = $value;
	}



	public function out() {
		foreach ($this->option_list as $row_id => $label) {
			$html_option_list[] = '<option value="'.$row_id.'" '.(($row_id == $this->value) ? 'selected="selected"' : '').'>'.$label.'</option>';
		}
		return $this->wrap('
			<label for="'.$this->name.'_select_filter">'.$this->label.'</label>
			<select name="'.$this->name.'" id="'.$this->name.'_select_filter">
				'.implode('', $html_option_list).'
			</select>
		');
	}



	public function modifyQuery($query) {
		if ($this->value) {
			$query->conditions[$this->name.'_filter'] = $this->field." = ".$this->value;
		}
	}
}
