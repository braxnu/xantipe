<?php
namespace xa\tool;

class Benchmark {

	protected static $timestamps = [];



	public static function start ($description = null) {
		$id = count(self::$timestamps);

		self::$timestamps[$id]['description'] = $description;
		self::$timestamps[$id]['time'] = self::microtime();

		return $id;
	}



	public static function microtime () {
		list($usec, $sec) = explode(' ', microtime());

		return ((float)$usec + (float)$sec);
	}



	public static function stop ($id) {
		return (self::microtime() - self::$timestamps[$id]['time']) * 1000;
	}
}
