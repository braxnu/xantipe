<?php
class XA_Filter_LiveSelect extends XA_Filter {

	public $value;
	public $field;
	public $display_list = array();
	public $source; // url zrodla danych


	public function __construct($name, $label, $field, $source_url, $value = '', $display_list = array()) {
		parent::__construct($name, $label);
		$this->field = $field;
		$this->value = $value;
		$this->display_list = $display_list;
		$this->source = $source_url;
	}


	public function out() {
		return $this->wrap(XA_XHTML_Form::getField(array(
			  'type' => 'liveselect'
			, 'name' => $this->name
			, 'source' => $this->source
			, 'label' => $this->label
			, 'value_list' => $this->display_list
			, 'wrap_tag' => 'div'
			, 'class_list' => array('liveselect') + $this->class_list
		)), false);
	}


	public function modifyQuery($query) {
		if ($this->value) {
			$query->conditions[$this->name.'_filter'] = $this->field." IN (".($this->value ?: 'NULL').")";
		}
	}
}
