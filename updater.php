<?php
namespace xa;

class Updater {



    public static function dump ($table_name, $file_path, $db = null) {
        $table = \xa::table($table_name);
        $list = $table->search();
        $pk_list = $table->get_primary_key_list();

        if (count($pk_list) === 1) {
            $pk_name = $pk_list[0];

            foreach ($list as $i => $row) {
                unset($list[$i][$pk_name]);
            }
        }

        file_put_contents($file_path, json_encode($list, \JSON_PRETTY_PRINT));
    }



    public static function sync ($table_name, $file_path, $db = null) {
        $json = file_get_contents($file_path);
        $list = json_decode($json, true);
        $table = \xa::table($table_name);

        foreach ($list as $row) {
            if (! $table->exists($row) ) {
                $table->insert($row);
            }
        }
    }
}
