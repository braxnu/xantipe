<?php
namespace xa;
/**
 * @package xa\HTML
 */

class HTML {

	public $class_list = [];
	public $id;
	public $tag;
	public $attribute_list = [];
	public $content = '';
    public static $self_closing = ['img', 'input', 'link'];



	public function __construct ($tag = null) {
		$this->tag = $tag ?: $this->tag ?: 'div';
	}



	public function out () {
		return
			$this->open().
            (
                in_array($this->tag, self::$self_closing)
                ? ''
                : $this->content.$this->close()
            );
	}



	public function open () {
		$node_list = [$this->tag];

		if ($this->id) {
			$node_list[] = 'id="'.$this->id.'"';
		}

		if ($this->class_list) {
			$node_list[] = 'class="'.implode(' ', $this->class_list).'"';
		}

		foreach ($this->attribute_list as $name => $value) {
            if ($value === true) {
                $node_list[] = $name;
            } elseif ($value || (string)$value === '0') {
                $node_list[] = $name.'="'.$value.'"';
            }
		}

		return '<'
            . implode(' ', $node_list)
            . (in_array($this->tag, self::$self_closing) ? '/' : '')
            . '>';
	}



	public function close () {
		return '</'.$this->tag.'>';
	}



	public function add_class ($class) {
		$this->class_list[] = $class;
	}



	public function remove_class ($class) {
        $key = array_search($class, $this->class_list);
		unset($this->class_list[$key]);
	}



	public static function render ($template_path, $var_list = []) {
		extract($var_list, EXTR_REFS | EXTR_SKIP);
        ob_start();
        include $template_path;

        return ob_get_clean();
	}
}
