<?php
function e () {
    header('Content-Type: text/plain; charset=UTF-8');
    $e = new \Exception("\n\n".\xa::dump(func_get_args())."\n\n");
    die("\n$e\n");
}



function x () {
    header('Content-Type: text/plain; charset=UTF-8');
    $e = new Exception("\n\n".\xa::dump(func_get_args(), true)."\n\n");
    die("\n$e\n");
}



function t () {
    $e = new Exception("\n\n".\xa::dump(func_get_args())."\n\n");

    $file = fopen(\LOG_PATH.'trace.log', 'a') or
        die('FATAL ERROR: cannot open '.\LOG_PATH.'trace.log'."\n");

    fwrite($file, "\n".str_repeat('-', 76)."\n".$e);
    fclose($file);
}



function filter_keys ($record, $key_list) {
    $return = [];

    foreach ($key_list as $key) {
        if (array_key_exists($key, $record)) {
            $return[$key] = $record[$key];
        }
    }

    return $return;
}



class xa {

    private static $db;
    public static $table_dict = [];
    public static $lang_id;



    public static function dump ($args, $parsable = false) {
        foreach ($args as $key => $arg) {
            if ($arg instanceof \xa\db\Query) {
                $args[$key] = \xa\db\query_renderer\PgSQL::out($arg);
            }
        }

        $func = $parsable ? 'var_export' : 'print_r';

        return $func(count($args) > 1 ? $args : current($args), 1);
    }



    public static function perm ($perm_name, $params = array()) {
        return \xa\Permission::can($perm_name, $params);
    }



    public static function db (
        $instance_name = 'default',
        $config_name = 'default'
    ) {
        return xa\DB::get($instance_name, $config_name);
    }



    public static function query ($sql) {
        return self::db()->query($sql);
    }



    public static function table ($table_name, $db_instance = 'default') {
        if (!isset(self::$table_dict[$table_name])) {
            if (is_string($db_instance)) {
                $db_instance = \xa\DB::get($db_instance);
            }

            $class_name = '\\xa\\db\\table\\'.$db_instance->type;
            self::$table_dict[$table_name] = new $class_name($table_name, $db_instance);
        }

        return self::$table_dict[$table_name];
    }



    public static function user () {
        return \xa\User::get();
    }



    public static function say ($text, $replacements = null, $language_id = null) {
        return \xa\Lang::say($text, $replacements, $language_id);
    }



    public static function option ($name, $value = null) {
        $table = \xa::table('option');

        if (is_null($value)) {
            // read
            $record = $table->find(['option_id' => $name]);

            if ($record) {
                return $record['value'];
            }
        } else {
            // create/update
            $table->save([
                'option_id' => $name,
                'value' => $value
            ]);
        }
    }



    public static function log ($params = [], $log_list = 'app') {
        \xa\Log::log($params, $log_list);
    }



    public static function url ($param_list = [], $for_html = false) {
        return \xa\Url::get($param_list, $for_html);
    }
}
