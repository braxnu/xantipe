<?php
namespace xa;

class Text {



	public static function ellipsis ($text, $max_length = 150) {
        if (strlen($text) <= $max_length) {
            return $text;
        }

        $text = substr($text, 0, $max_length - 3);
        $last_space_pos = strrpos($text, ' ');

        return rtrim(substr($text, 0, $last_space_pos), '?!"\'.,;:').'...';
    }
}
