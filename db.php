<?php
namespace xa;

/**
 * Abstract database connection class.
 *
 * Connection credentials should be defined as array inside
 * of \Config::$db[$config_name] (where default $config_name = 'default').
 * Depending on database drive type, credentials may look like:
 * <code>
 *     [
 *         'user' => '',
 *         'password' => '',
 *         'host' => '',
 *         'name' => ''
 *     ]
 * </code>
 *
 * @example \xa\DB::get()->query('SELECT 1')->val();
 */
abstract class DB {

    /**
     * Dictionary of name-indexed \xa\DB (database interface) instances.
     * @var \xa\DB[]
     */
    private static $instance_dict = [];

    /**
     * Transaction level for current connection. Works as a stack for
     * transactions started in the application. Real database transaction
     * is started and ended only when transaction level is equal to 0.
     * @var int
     */
    protected $transaction_level = 0;

    /**
     * DB connection resource.
     * @var resource
     */
    protected $connection;

    /**
     * DB connection resource.
     * @var resource
     * @todo change name to $last_insert_id
     */
    protected $insert_id;

    public $type;



    // Constructor is protected to avoid direct instantiation. Use DB::get()
    // instead.
    abstract protected function __construct ($config);

    abstract public function query ($query);
    abstract public function escape_string ($string);



    /**
     * Returns last auto-generated primary key value inserted on current
     * connection.
     * @return int
     */
    public function last_id () {
        return $this->insert_id;
    }



    /**
     * Starts new transaction for current connection and raises internal
     * transaction level.
     * @return object Self
     */
    public function begin () {
        $this->transaction_level++;
        $this->begin_trans();
    }



    /**
     * Ends transaction on current connection or lowers internal transaction
     * level if transactin started more than once.
     * @param bool $commit Tells if we want to commit the transaction. If false
     *                     or undefined, transaction is rolled back.
     * @todo Consider situation where rollback is called at non zero transaction
     *       level. Should the whole transaction stack be cancelled and
     *       exception thrown?
     */
    public function end ($commit = false) {
        $this->transaction_level--;

        if ($this->transaction_level === -1) {
            throw new \Exception('Negative transaction level');
        }

        if ($this->transaction_level === 0) {
            $this->end_trans($commit);
        }
    }



    /**
     * Returns escaped version of given string to be used in SQL query.
     * @param string $string String to be prepared.
     * @return string Escaped string.
     */
    public function esc ($string) {
        return $this->escape_string($string);
    }



    /**
     * Returns database (connection) instance. If connection was not
     * @param string $instance_name Instance (connection) name.
     * @param string $config_name Database credentials set name.
     * @return string Escaped string.
     */
    public static function get (
        $instance_name = 'default',
        $config_name = 'default'
    ) {
        if (empty(self::$instance_dict[$instance_name])) {
            if (is_array(current(Config::$db))) {
                $config = Config::$db[$config_name];
            } else {
                $config = Config::$db;
            }

            $class_name = '\\xa\\Db\\Driver\\'.$config['type'];
            self::$instance_dict[$instance_name] = new $class_name($config);
        }

        return self::$instance_dict[$instance_name];
    }



    /**
     * Returns database table object associated with this database connection.
     * @param string $table_name Table name.
     * @return \xa\DB\Table Table object.
     */
    public function table ($table_name) {
        // TODO cache instances
        $class_name = "\\xa\\DB\\Table\\{$this->type}";

        return new $class_name($table_name, $this);
    }
}
