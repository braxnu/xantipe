<?php
class XA_Filter_Checkbox extends XA_Filter {

	public $value;
	public $condition_list;



	public function __construct($name, $label, $condition_list = array(), $value = null) {
		parent::__construct($name, $label);
		$this->condition_list = $condition_list;
		$this->value = $value;
	}



	public function out() {
		$checked = $this->value ? ' checked="checked"' : '';
		return $this->wrap('
			<label for="'.$this->name.'_select_filter">'.$this->label.'</label>
			<input type="checkbox" name="'.$this->name.'" id="'.$this->name.'_select_filter" value="1" '.$checked.'/>
		');
	}



	public function modifyQuery($query) {
		if ($this->value) {
			foreach ($this->condition_list as $condition_name => $condition) {
				$query->conditions[$this->name.'_'.$condition_name.'_filter'] = $condition;
			}
		}
	}
}
