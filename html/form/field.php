<?php
namespace xa\html\form;

abstract class Field {

    public $name;
    public $type;
    public $value;
    public $label;
    public $hint;
    public $id;
    public $form;



    abstract public function out ();



    public function __construct ($params = []) {
        foreach (array_keys($params) as $param_name) {
            if (property_exists($this, $param_name)) {
                $this->$param_name = $params[$param_name];
            }
        }

        if (empty($this->id)) {
            $this->id = $this->get_id();
        }
    }



    public static function get ($params = []) {
        $type = empty($params['type']) ? 'text' : $params['type'];
        $class_name = '\\XA\\HTML\\Form\\Field\\'.$type;

        return new $class_name($params);
    }



    protected function get_id () {
        if (
            isset($this->form)
            &&
            !empty($this->form->id)
        ) {
            $form_id_part = $this->form->id.'_';
        } else {
            $form_id_part = '';
        }

        return $form_id_part.$this->name.'_field';
    }
}
