<?php
namespace xa\html\form\field;

class Date extends \xa\html\form\Field {



	public function out () {
		$input = new \xa\HTML('input');
		$input->id = $this->get_id();
		$input->attribute_list['size'] = 10;
		$input->attribute_list['type'] = 'text';
		$input->attribute_list['name'] = $this->name;
		$input->class_list[] = 'date';

		if (isset($this->value)) {
			$input->attribute_list['value'] = $this->value;
		}

		return $input->out();
	}

	/*
	$size = !empty($field['size']) ? $field['size'] : 35;
	$max = !empty($field['max']) ? $field['max'] : null;

	$return .= '<label for="'.$form_id.'_form_'.$field_name.'_field">'
		.(isset($field['label']) ? $field['label'] : $field_name).'</label>';

	$return .= '<input
		'.(isset($field['class']) ? ' class="'.$field['class'].'"' : '').'
		size="'.$size.'"
		maxlength="'.($max ? $max : $size).'"
		type="'.$field['type'].'"
		name="'.$field_name.'"
		id="'.$form_id.'_form_'.$field_name.'_field"
		value="'.(isset($field['value']) ? $field['value'] : '').'"'
		.(!empty($field['disabled']) ? ' disabled="disabled"' : '').'
		/>';
	*/
}
