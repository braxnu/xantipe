<?php
namespace xa\html\form\field;

class Checkbox extends \xa\html\form\Field {

    public $value;



    public function __construct ($params = []) {
        $this->value = !empty($params['value']);

        parent::__construct($params);
    }



    public function out () {
        $input = new \xa\HTML('input');
        $input->id = $this->get_id();
        $input->attribute_list['type'] = 'checkbox';
        $input->attribute_list['name'] = $this->name;
        $input->attribute_list['value'] = $this->name;

        if ($this->value) {
            $input->attribute_list['checked'] = 'checked';
        }

        return $input->out();
    }
}
