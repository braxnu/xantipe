<?php
namespace xa\html\form\field;

class Select extends \xa\html\form\Field {

    public $option_list = [];



	public function __construct ($params = []) {
        parent::__construct($params);
        
        if (!empty($params['option_list'])) {
            $this->option_list = $params['option_list'];
        }
    }



	public function out () {
		$input = new \xa\HTML('select');
		$input->id = $this->get_id();
		$input->attribute_list['name'] = $this->name;
        
        $content = '';
        
        foreach ($this->option_list as $opt_value => $opt_label) {
            $option_html = new \xa\HTML('option');
            $option_html->attribute_list['value'] = $opt_value;
            $option_html->content = $opt_label;
            
            if ($this->value == $opt_value) {
                $option_html->attribute_list['selected'] = true;
            }
            
            $input->content .= "\n".$option_html->out();
        }

		return $input->out();
	}

	/*
	$size = !empty($field['size']) ? $field['size'] : 35;
	$max = !empty($field['max']) ? $field['max'] : null;

	$return .= '<label for="'.$form_id.'_form_'.$field_name.'_field">'
		.(isset($field['label']) ? $field['label'] : $field_name).'</label>';

	$return .= '<input
		'.(isset($field['class']) ? ' class="'.$field['class'].'"' : '').'
		size="'.$size.'"
		maxlength="'.($max ? $max : $size).'"
		type="'.$field['type'].'"
		name="'.$field_name.'"
		id="'.$form_id.'_form_'.$field_name.'_field"
		value="'.(isset($field['value']) ? $field['value'] : '').'"'
		.(!empty($field['disabled']) ? ' disabled="disabled"' : '').'
		/>';
	*/
}
