<?php
namespace xa\html\form\field;

class Textarea extends \xa\html\form\Field {

	public $cols;
	public $rows;



	public function out () {
		$input = new \xa\HTML('textarea');
		$input->id = $this->get_id();
		$input->attribute_list['name'] = $this->name;

		if (!empty($this->cols)) {
			$input->attribute_list['cols'] = $this->cols;
		}

		if (!empty($this->rows)) {
			$input->attribute_list['rows'] = $this->rows;
		}

		if (isset($this->value)) {
			$input->content = $this->value;
		}

		return $input->out();
	}
}
