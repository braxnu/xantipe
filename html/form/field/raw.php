<?php
namespace xa\html\form\field;

class Raw extends \xa\html\form\Field {

	public $element;
	public $content;



	public function out () {
		if ($this->element) {
			$this->element->content = $this->content;

			return $this->element->out();
		} else {
			return $this->content;
		}
	}
}
