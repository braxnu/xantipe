<?php
namespace xa\html\form\field;

class Submit extends \xa\html\form\Field {

    public $disabled = false;



    public function out () {
        $input = new \xa\HTML('input');
        $input->id = $this->name.'_field';
        $input->attribute_list['type'] = 'submit';

        if ($this->disabled) {
            $input->attribute_list['disabled'] = 'disabled';
        }

        if (isset($this->value)) {
            $input->attribute_list['value'] = $this->value;
        }

        return $input->out();
    }
}
