<?php
namespace xa\html\form\field;

class Text extends \xa\html\form\Field {

	public $autocomplete;
	public $maxlength;
	public $size;



	public function __construct ($params = []) {
		if (!empty($params['size'])) {
			$this->maxlength = $params['size'];
			$this->size = $params['size'];
		}

		parent::__construct($params);
	}



	public function out () {
		$input = new \xa\HTML('input');
		$input->id = $this->get_id();
		$input->attribute_list['type'] = 'text';
		$input->attribute_list['name'] = $this->name;

		if (isset($this->value)) {
			$input->attribute_list['value'] = $this->value;
		}

		if (isset($this->maxlength)) {
			$input->attribute_list['maxlength'] = $this->maxlength;
		}

		if (isset($this->size)) {
			$input->attribute_list['size'] = $this->size;
		}

		if (isset($this->autocomplete)) {
			$input->attribute_list['autocomplete'] =
				$this->autocomplete ? 'on' : 'off';
		}

		return $input->out();
	}

	/*
	$size = !empty($field['size']) ? $field['size'] : 35;
	$max = !empty($field['max']) ? $field['max'] : null;

	$return .= '<label for="'.$form_id.'_form_'.$field_name.'_field">'
		.(isset($field['label']) ? $field['label'] : $field_name).'</label>';

	$return .= '<input
		'.(isset($field['class']) ? ' class="'.$field['class'].'"' : '').'
		size="'.$size.'"
		maxlength="'.($max ? $max : $size).'"
		type="'.$field['type'].'"
		name="'.$field_name.'"
		id="'.$form_id.'_form_'.$field_name.'_field"
		value="'.(isset($field['value']) ? $field['value'] : '').'"'
		.(!empty($field['disabled']) ? ' disabled="disabled"' : '').'
		/>';
	*/
}
