<?php
namespace xa\html\form\field;

class Hidden extends \xa\html\form\Field {



	public function out () {
		$id = $this->name.'_field';

		$input = new \xa\HTML('input');
		$input->id = $id;
		$input->attribute_list['type'] = 'hidden';
		$input->attribute_list['name'] = $this->name;
		$input->attribute_list['value'] = $this->value;

		return $input->out();
	}

	/*
		id="'.$form_id.'_form_'.$field_name.'_field"
	*/
}
