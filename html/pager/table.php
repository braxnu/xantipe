<?php
namespace xa\html\pager;

class Table extends \xa\html\Pager {

	public $table;



    public function __construct (\xa\db\Pager $pager, $param_list = []) {
        parent::__construct($pager, $param_list);
		$this->table = new \xa\html\Table();
    }



    public function out () {
		$this->table->row_list = $this->pager->get_rows();

		$pagenav_html = parent::get_page_nav(
			$this->pager->get_page_count(),
			$this->param_list['page'],
			$this->param_list,
			$this->width
		);

		return $pagenav_html
			. $this->table->out()
			. $pagenav_html;
	}
}
