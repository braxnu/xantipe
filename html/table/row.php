<?php
namespace xa\html\table;

class Row extends \xa\HTML {

	public $cell_list = [];
	public $tag = 'tr';



	public function out () {
		$content_html = [];

		foreach ($this->cell_list as $cell) {
			$content_html[] = "\n".$cell->out();
		}

		return
			  $this->open()
			. implode('', $content_html)
			. $this->close();
	}
}
