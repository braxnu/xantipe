<?php
namespace xa\html\table\column;

class Bool extends \xa\html\table\Column {



	public function cell ($row) {
		$cell = new \xa\html\table\Cell();
		$cell->addClass($this->name);
		$cell->content = \xa::say($row[$this->name] ? 'Tak' : 'Nie');

		return $cell;
	}
}
