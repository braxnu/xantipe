<?php
class XA_Table_Field_Dict extends XA_Table_Field {

	public $option_list = array();


	public function __construct($name, $label, $option_list) {
		parent::__construct($name, $label);
		$this->option_list = $option_list;
	}


	public function out($row) {
		return '<td class="'.implode(' ', $this->class_list).'">'.(isset($this->option_list[$row[$this->name]]) ? $this->option_list[$row[$this->name]] : '').'</td>';
	}
}
