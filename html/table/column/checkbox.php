<?php
class XA_Table_Field_Checkbox extends XA_Table_Field {

	public $listview;
	

	public function __construct($name, $label, $listview) {
		parent::__construct($name, $label);
		$this->listview = $listview;
	}



	public function out($row) {
		$pk_chunk_list = array();
		foreach ($this->listview->pk_list as $pk) {
			$pk_chunk_list[] = $row[$pk];
		}
		return '<td class="'.implode(' ', $this->class_list).'"><input type="checkbox" name="'.implode('_', $pk_chunk_list).'" /></td>';
	}
}
