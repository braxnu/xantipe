<?php
namespace xa\html\table\column;

class Simple extends \xa\html\table\Column {



	public function cell ($row) {
		$cell = new \xa\html\table\Cell();
		$cell->add_class($this->name);
		$cell->content = nl2br( $row[$this->name] );

		return $cell;
	}
}
