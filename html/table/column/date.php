<?php
namespace xa\html\table\column;

class Date extends \xa\html\table\Column {



	public function cell ($row) {
		$cell = new \xa\html\table\Cell();
		$cell->addClass($this->name);
		$cell->content = date('Y-m-d H:i', $row[$this->name]);

		return $cell;
	}
}
