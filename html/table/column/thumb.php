<?php
class XA_Table_Field_Thumb extends XA_Table_Field {

	public $size;
	public $param_dict = array();



	public function __construct ($name, $label, $size = 'thumb', $param_dict = array()) {
		parent::__construct($name, $label);
		$this->size = $size;
		$this->class_list[] = 'thumb';
		$this->param_dict = $param_dict;
	}



	public function out($row) {
		$param_dict = array(
			  'action' => 'image'
			, 'size' => $this->size
			, 'slug' => $row['slug']
			, 'file_id' => $row['file_id']
		) + $this->param_dict;

		return '
		<td class="'.implode(' ', $this->class_list).'">
			<img src="'.xa::url($param_dict).'" />
			<span style="display: none;" class="file_id">'.$row['file_id'].'</span>
		</td>
		';
	}
}
