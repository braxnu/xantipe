<?php
namespace xa\html\table\column;

class Options extends \xa\html\table\Column {

	protected $option_list = [];
	protected $pk_name_list = [];



	public function __construct ($name, $label = null, $options = []) {
		parent::__construct($name, $label);

		$this->option_list = empty($options['option_list']) ? [] : $options['option_list'];
		$this->pk_name_list = empty($options['pk_name_list']) ? [] : $options['pk_name_list'];
	}



	/**
	 * Returns array of url parameters with values for given row.
	 */
	protected function get_param_list ($option_param_list, $row) {
		$param_list = $option_param_list;

		foreach ($this->pk_name_list as $pk_name) {
			$param_list[$pk_name] = $row[$pk_name];
		}

		return $param_list;
	}



	/**
	 * Returns option list for given row. This method may be overriden to obtain
	 * different sets of options depending on row data.
	 */
	protected function get_option_list ($row) {
		return $this->option_list;
	}



    public function add_option ($label, $param_list) {
        $this->option_list[] = [
            'label' => $label,
            'param_list' => $param_list
        ];
    }



	protected function get_option_html ($option, $row) {
		// array copy
		$param_list = $option['param_list'];

		foreach ($this->pk_name_list as $pk_name) {
			$param_list[$pk_name] = $row[$pk_name];
		}

		return '<a class="button"
			href="'.\xa::url($param_list).'"
			title="'.$option['label'].'">'.$option['label'].'</a>';
	}



	protected function out ($row) {
		$option_html_list = [];

		foreach ($this->get_option_list($row) as $option) {
			$option_html_list[] = $this->get_option_html($option, $row);
		}

		return implode(' ', $option_html_list);
	}



	public function cell ($row) {
		$cell = new \xa\html\table\Cell();
		$cell->add_class('option');
		$cell->content = $this->out($row);

		return $cell;
	}
}
