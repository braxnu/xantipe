<?php
namespace xa\html\table\column;

class Custom extends \xa\html\table\Column {

	public $content_function;



	public function __construct ($name, $label, $content_function) {
		parent::__construct($name, $label);
		$this->content_function = $content_function;
	}



	public function cell ($row) {
		$cell = new \xa\html\table\Cell();
		$func = $this->content_function;
		$cell->content = $func($row);

		return $cell;
	}
}
