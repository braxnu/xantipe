<?php
namespace xa\html\table;

abstract class Column {

	public $name;
	public $label;
	public $position;



	abstract public function cell ($row);



	public function __construct ($name, $label = null) {
		$this->name = $name;
		$this->label = $label;
	}
}
