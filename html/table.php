<?php
namespace xa\html;

class Table extends \xa\HTML {

    public $tag = 'table';
    public $row_list = [];
    public $label_list = [];
    public $column_list = [];
    public $modifier_list = [];
    public $force_create_default_columns = false;
    protected $last_column_position = 0;



    public function add_modifier ($function) {
        $this->modifier_list[] = $function;
    }



    public function add_column ($column, $label = null) {
        if (is_string($column)) {
            $column = new \xa\html\table\column\Simple($column, $label);
        }

        if (!$column->position) {
            $column->position = $this->last_column_position++;
        }

        $this->column_list[$column->name] = $column;
    }



    public function add_column_list ($column_list) {
        foreach ($column_list as $column => $label) {
            $this->add_column($column, $label);
        }
    }



    public function create_default_columns () {
        if (!$this->row_list) {
            return;
        }

        foreach (array_keys(current($this->row_list)) as $column_name) {
            $this->add_column($column_name, $column_name);
        }
    }



    public function out () {
        if (
            !$this->column_list
            ||
            $this->force_create_default_columns
        ) {
            $this->create_default_columns();
        }


        // sorting columns by position

        $column_list = [];

        foreach ($this->column_list as $column) {
            $column_list[$column->position] = $column;
        }

        ksort($column_list);


        $toggle = false;
        $html_row_list = [];

        foreach ($this->row_list as $row) {
            $html_row = new \xa\html\table\Row();

            foreach ($this->modifier_list as $modifier) {
                $row = $modifier($row);
            }

            foreach ($column_list as $column) {
                $html_row->cell_list[] = $column->cell($row);
            }

            if ($toggle) {
                $html_row->add_class('toggle');
            }

            $html_row_list[] = "\n".$html_row->out();
            $toggle = !$toggle;
        }


        $thead_html = '<thead><tr>';

        foreach ($column_list as $column) {
            $field_name = $column->name;

            if (!isset($this->label_list[$field_name])) {
                $this->label_list[$field_name] = $column->label;
            }

            if (is_object($this->label_list[$field_name])) {
                $label = $this->label_list[$field_name];
                $thead_html .= $label->out();
            } else {
                $thead_html .= '<th class="'.$field_name.'">'.$this->label_list[$field_name].'</th>';
            }
        }

        $thead_html .= '</tr></thead>';


        return $this->open()
            . $thead_html
            . '<tbody>'.implode("\n", $html_row_list).'</tbody>'
            . $this->close();
    }



    /*
     * Sets the content of header field for a column.
     * @param string $field_name Name of the column.
     * @param mixed $params May be a string, which will be used as header. If
     * array of URL params given, this array will be used to create a link
     * for the header. If view instance given, this instance output will be
     * used for header.
     */
    public function set_label ($field_name, $params, $text = null) {
        if (is_object($params)) {
            $label = $params;
        } else {
            $label = new \xa\HTML('th');

            if (is_string($params)) {
                $label->content = $params;
            } elseif (is_array($params)) {
                $link = new \xa\HTML('a');
                $link->content = $text;
                $link->attribute_list['href'] = \xa::url($params);

                $label->content = $link->out();
            }
        }

        $label->add_class($field_name);
        $this->label_list[$field_name] = $label;
    }
}

