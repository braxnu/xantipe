<?php
class XA_XHTML_Select extends XA_XHTML {

	public $option_list = array();
	public $active;
	public $value_column;
	public $display_column;
	public $name;


	public function __construct($option_list, $name, $value_column = null, $display_column = null) {
		$this->option_list = $option_list;
		$this->name = $name;
		list($default_value_column, $default_display_column) = array_keys(current($option_list));
		$this->value_column = ($value_column ? $value_column : $default_value_column);
		$this->display_column = ($display_column ? $display_column : $default_display_column);
	}



	public function getOutput() {

		if (!$this->active) {
			$first = current($this->option_list);
			$this->active = $first[$this->value_column];
		}

		$option_list = array();
		foreach ($this->option_list as $option) {
			$option_list[] = '<option '.((strval($option[$this->value_column]) === strval($this->active)) ? 'selected="selected"' : '').' value="'.$option[$this->value_column].'">'.$option[$this->display_column].'</option>';
		}
		return '
			<select name="'.$this->name.'" id="'.$this->id.'" class="'.implode(' ', $this->classes).'">
				'.implode('', $option_list).'
			</select>
		';
	}
}
