<?php
namespace xa\html;

abstract class Pager {

	public $pager;
	public $param_list = [];
	public $width = 5;



	abstract public function out ();



	public function __construct (\xa\db\Pager $pager, $param_list = []) {
		$this->pager = $pager;
		$param_list['page'] = $pager->page_number;
		$this->param_list = $param_list;
	}



	protected static function get_page_nav (
		$page_count,
		$current_page,
		$param_list,
		$width
	) {
		$html = '<div class="pagenav">'.
			'<span>'.\xa::say('Strona').'</span>'.
			'<ul>';

		if ($current_page > 1) {
			// "Previous" button
			$param_list['page'] = $current_page - 1;

			$html .= "\n".'<li class="previous">'.
				'<a href="'.\xa::url($param_list).'">'.
				\xa::say('Poprzednia').
				'</a></li>';
		}

		for ($page_loop = 1; $page_loop <= $page_count; $page_loop++) {
			if (
				$current_page >= $page_loop - $width
				&&
				$current_page <= $page_loop + $width
			) {
				$param_list['page'] = $page_loop;

				$html .= '<li'.
					($page_loop == $current_page ? ' class="active"' : '').
					'><a href="'.
					\xa::url($param_list)
					.'">'.$page_loop.'</a></li>';
			}
		}

		if ($current_page < $page_count) {
			$param_list['page'] = $current_page + 1;

			$html .= '<li><a href="'.\xa::url($param_list)
				.'">'.\xa::say('Następna').'</a></li>';
		}

		$html .= '</ul></div>';

		return $html;
	}
}
