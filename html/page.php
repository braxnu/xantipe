<?php
namespace xa\html;

class Page extends \xa\HTML {

	public $title = \SITE_NAME;
	public $generator = 'Xantipe';
	public $css_list = [];
	public $inline_script_list = [];
	public $script_list = [];
	public $encoding = 'UTF-8';
	public $layout = 'default';
	public $template = 'default';
	public $content = [];
	public $description = '';
	public $lang = null;
	public $head_tag_list = [];



    public function add_script ($src) {
        $this->head_tag_list[] =
            '<script type="text/javascript" src="'.$src.'"></script>';
    }



    public function add_inline_script ($script) {
        $this->head_tag_list[] =
            '<script type="text/javascript">'.$script.'</script>';
    }



	public function out () {
		global $user, $action;

		// TODO path to be injected as dependency
		$template_path = \MODULE_PATH.'templates/'.$this->template.'.php';

		$c =& $this->content;
		extract($this->content, EXTR_REFS | EXTR_SKIP);

		foreach ($this->css_list as $sheet) {
			if (is_array($sheet)) {
				$this->head_tag_list[] =
					'<link rel="stylesheet" href="'.
					$sheet['href'].
					'" type="text/css"'.
					(
						isset($sheet['media'])
						? ' media="'.$sheet['media'].'"'
						: ''
					).' />';
			} else {
				$this->head_tag_list[] = '<link rel="stylesheet" href="'
					.$sheet.'" type="text/css" />';
			}
		}

		foreach ($this->script_list as $script) {
			$this->head_tag_list[] = '<script type="text/javascript" src="'
				.$script.'"></script>';
		}

		foreach ($this->inline_script_list as $script) {
			$this->head_tag_list[] = '<script type="text/javascript">'
				.$script.'</script>';
		}

		echo '<!DOCTYPE html>
<html'.($this->lang ? ' lang="'.$this->lang.'"' : '').'>
	<head>
		<meta name="generator" content="'.$this->generator.'" />
		<meta http-equiv="Content-Type" content="text/html; charset='.$this->encoding.'" />
		<title>'.$this->title.'</title>
		<link rel="shortcut icon" type="image/x-icon" href="'.\xa::url().'favicon.ico" />
		'.implode("\n\t\t", $this->head_tag_list).'
		<meta name="description" content="'.htmlspecialchars($this->description).'">
	</head>
	<body class="'.implode(' ', $this->class_list).'">
';

		require \MODULE_PATH.'templates/layouts/'.$this->layout.'.php';

		echo '
	</body>
</html>
';
	}
}
