<?php
// TODO separate form logic from HTML renderer
namespace xa\html;

use \xa\In;

class Form extends \xa\HTML {

    public $tag = 'form';
    public $field_list = [];
    public $title;
    public $action;
    public $has_files = false;
    public $error_list = [];
    public $template_name;
    public $method = 'post';
    public static $set_field_type_list = ['set', 'radio'];
    public $last_validated_data = [];



    public function set_labels ($label_list) {
        $field_name_list = array_keys($this->field_list);

        foreach ($label_list as $field_name => $label) {
            if (in_array($field_name, $field_name_list)) {
                $this->field_list[$field_name]['label'] = $label;
            }
        }
    }



    // TODO refactor
    public function set_positions ($position_list) {
        // musi być if bo automat czasem daje pustego arraya
        if (!$position_list) {
            return;
        }

        $first_value = current($position_list);

        if (strval($first_value) !== strval(intval($first_value))) {
            // dostalismy tablicę z poukładanymi w kolejności nazwami kluczy,
            // więc indeksujemy tak jak leżą wartosci w tablicy
            $position_list = array_combine(
                  $position_list
                , range(1, count($position_list))
            );
        }

        foreach ($position_list as $field_name => $position) {
            if (isset($this->field_list[$field_name])) {
                $this->field_list[$field_name]['position'] = $position;
            }
        }
    }



    public function get_values () {
        $form_data = [];

        foreach ($this->field_list as $name => $field) {
            if (isset($field['value'])) {
                $form_data[$name] = $field['value'];
            }
        }

        return $form_data;
    }



    public function set_values ($value_list) {
        foreach (array_keys($this->field_list) as $field_name) {
            if (array_key_exists($field_name, $value_list)) {
                $this->field_list[$field_name]['value'] = $value_list[$field_name];
            }
        }
    }



    protected function get_sorted_fields () {
        $result = [];
        $temp = [];
        $last_position = 0;

        foreach ($this->field_list as $field_name => $field) {
            $position = empty($field['position'])
                ? $last_position + 1
                : $field['position'];

            while (!empty($temp[$position])) {
                $position++;
            }

            $temp[$position] = $field;
            $last_position = $position;
        }

        ksort($temp);

        foreach ($temp as $field) {
            $result[$field['name']] = $field;
        }

        return $result;
    }



    public function validate ($data = []) {
        $this->last_validated_data = $data;

        foreach ($this->field_list as $field_name => $field) {
            if (empty($field['rule_list'])) continue;

            foreach ($field['rule_list'] as $rule_name => $rule) {
                if (is_array($rule)) {
                    if (!empty($rule['function'])) {
                        $function = $rule['function'];

                        if (!$function($field_name, $data)) {
                            $this->error_list[$field_name][] = $rule['message'];
                        }
                    } elseif (!empty($rule['object'])) {
                        $obj = $rule['object'];

                        if (!$obj->validate($field_name, $data)) {
                            $this->error_list[$field_name][] = $rule['message'];
                        }
                    }
                } else {
                    $valid = true;

                    switch ($rule_name) {
                        case 'not_empty':
                            $valid = !empty($data[$field_name]);
                            break;

                        case 'email':
                            $valid = In::is_email($data[$field_name]);
                            break;

                        case 'int':
                            $valid = In::is_int($data[$field_name]);
                            break;

                        case 'float':
                            $valid = In::is_float($data[$field_name]);
                            break;

                        case 'nat':
                            $valid = In::is_nat($data[$field_name]);
                            break;

                        case 'symbol':
                            $valid = In::is_symbol($data[$field_name]);
                            break;

                        default:
                            throw new \Exception(
                                "There is no '$rule_name' built-in validation".
                                " rule. (Used for field '$field_name')"
                            );
                    }

                    if (!$valid) {
                        $this->error_list[$field_name][] = $rule;
                    }
                }
            }
        }

        return !$this->error_list;
    }



    public function save () {
        if (!empty($this->last_validated_data['tmp_id'])) {
            $tmp_id = $this->last_validated_data['tmp_id'];
        } else {
            $tmp_id = md5(time().mt_rand());
        }

        $_SESSION['form_'.$tmp_id] = [
            'values' => $this->last_validated_data,
            'errors' => $this->error_list
        ];

        return $tmp_id;
    }



    public function load ($tmp_id = null) {
        if (!isset($tmp_id)) {
            $tmp_id = $_GET;
        }

        if (is_array($tmp_id)) {
            if (empty($tmp_id['tmp_id'])) {
                return;
            }

            $tmp_id = $tmp_id['tmp_id'];
        }

        if (empty($_SESSION['form_'.$tmp_id])) {
            return;
        }

        $form_data = $_SESSION['form_'.$tmp_id];

        if (!empty($form_data['values'])) {
            $this->set_values($form_data['values']);
        }

        if (!empty($form_data['errors'])) {
            $this->error_list = $form_data['errors'];
        }

        $this->field_list['tmp_id'] = [
            'type' => 'hidden',
            'value' => $tmp_id
        ];
    }



    protected function template_out () {
        ob_start();
        include \MODULE_PATH.'templates/forms/'.$this->template_name.'.php';
        return ob_get_clean();
    }



    protected function default_out () {
        $content = '';

        if ($this->title) {
            $form_title = new \xa\HTML('h3');
            $form_title->content = $this->title;
            $content .= $form_title->out()."\n";
        }

        $this->field_list = $this->get_sorted_fields();

        foreach (array_keys($this->field_list) as $field_name) {
            $content .= "\n".$this->out_field($field_name, true);
        }

        return $content;
    }



    public function out () {
        if ($this->action) {
            $this->attribute_list['action'] = $this->action;
        }

        if ($this->method) {
            $this->attribute_list['method'] = $this->method;
        }

        if ($this->has_files) {
            $this->attribute_list['enctype'] = 'multipart/form-data';
        }

        foreach (array_keys($this->field_list) as $field_name) {
            $this->field_list[$field_name]['name'] = $field_name;
        }

        $this->content .= $this->template_name
            ? $this->template_out()
            : $this->default_out();

        return parent::out();
    }



    public function out_field ($field_name, $wrapped = false) {
        $field_params = $this->field_list[$field_name];
        $field_params['name'] = $field_name;
        $field_params['form'] = $this;

        if (empty($field_params['type'])) {
            $field_params['type'] = 'text';
        }

        if (in_array($field_params['type'], ['raw', 'submit'])) {
            $field_params['label'] = false;
        }

        $field = \xa\html\form\Field::get($field_params);

        if (!$wrapped) {
            return $field->out();
        }

        $wrap = new \xa\HTML();
        $wrap->add_class($field->name);
        $wrap->add_class('field');
        $wrap->add_class($field_params['type']);

        if (!empty($field_params['class_list'])) {
            foreach ($field_params['class_list'] as $class) {
                $wrap->add_class($class);
            }
        }

        if ($field_params['type'] === 'hidden') {
            $wrap->attribute_list['style'] = 'display: none;';
        } else {
            if (
                !isset($field_params['label'])
                ||
                $field_params['label'] !== false
            ) {
                $label = new \xa\HTML('label');
                $label->add_class('field');
                $label->attribute_list['for'] = $field->id;
                $label->content = $field->label;
            }

            if (
                isset($label)
                &&
                $field_params['type'] !== 'checkbox'
            ) {
                $wrap->content .= $label->out();
            }

            if (!empty($this->error_list[$field_name])) {
                $err_wrap = new \xa\HTML();
                $err_wrap->add_class('error_list');

                $err_html_list = [];

                foreach ($this->error_list[$field_name] as $err_name => $err_message) {
                    if (is_array($err_message)) {
                        $err_message = $err_message['message'];
                    }

                    $err_html = new \xa\HTML();
                    $err_html->content = $err_message;
                    $err_html_list[] = $err_html->out();
                }

                $err_wrap->content = implode("\n", $err_html_list);
                $wrap->content .= $err_wrap->out();
            }

            if (!empty($field_params['hint'])) {
                $hint_el = new \xa\HTML();
                $hint_el->add_class('hint');
                $hint_el->content = $field_params['hint'];
                $wrap->content .= $hint_el->out();
            }
        }

        $wrap->content .= $field->out();

        if (
            isset($label)
            &&
            $field_params['type'] === 'checkbox'
        ) {
            $wrap->content .= $label->out();
        }

        return $wrap->out();
    }
}
