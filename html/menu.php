<?php
namespace xa\html;

class Menu extends \xa\HTML {

    public $tag = 'ul';
    protected $item_list = [];
    protected $active_item_index = null;



    public function __construct($tag = null) {
        parent::__construct($tag);
        $this->add_class('menu');
    }



    public function set_active ($index) {
        $this->active_item_index = $index;
    }



    public function out () {
        $item_html_list = [];

        foreach ($this->item_list as $index => $item) {
            $item_html_list[] = "\n"
                . '<li class="'
                . ($index === $this->active_item_index ? 'active ' : '')
                . implode(' ', $item['class_list'])
                . '"><a href="'.$item['url'].'">'
                . $item['text']
                . '</a></li>';
        }

        return $this->open()
            . implode('', $item_html_list)
            . $this->close();
    }



    public function remove_item ($index) {
        unset($this->item_list[$index]);
    }



    public function add_item ($index, $url, $text = null, $class_list = []) {
        if (is_string($class_list)) {
            $class_list = explode(' ', $class_list);
        }

        $class_list[] = $index;

        $this->item_list[$index] = [
            'url' => $url,
            'text' => $text ?: $url,
            'class_list' => $class_list
        ];
    }
}
