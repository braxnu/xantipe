<?php
namespace xa;

class User {

	private static $instance;



	public function get_id () {
		return null;
	}



	public static function init ($instance) {
		self::$instance = $instance;
	}



	public static function id () {
		if (self::$instance) {
			// initialised
			return self::$instance->get_id();
		}
	}



	public static function get () {
		return self::$instance;
	}
}
