<?php
class XA_ListView_Table_Tree extends XA_ListView_Table {

	public $count = null;
	public $indent_field = 'name';


	public function prepareRows() {
		$rows = XA_Tree::depthList(XA_Tree::get($this->row_list));
		foreach ($rows as $row_id => $row) {
			$rows[$row_id][$this->indent_field] = str_repeat('&nbsp;', $rows[$row_id]['depth'] * 5).$rows[$row_id][$this->indent_field];
		}
		$this->row_list = $rows;
	}
}
