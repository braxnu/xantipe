<?php
define('xa\\LIB_PATH', dirname(__FILE__).'/');

function xa_autoload ($class_name) {
    $class_name_chunks = explode('\\', strtolower($class_name));

    if ($class_name_chunks[0] !== 'xa') {
        return;
    } elseif (count($class_name_chunks) === 1) {
        // 'XA' class
        include 'xa.php';
        return;
    }

    array_shift($class_name_chunks);
    $file_path = \xa\LIB_PATH.implode('/', $class_name_chunks).'.php';

    if (!file_exists($file_path)) {
        return;
    }

    include $file_path;
}

spl_autoload_register('xa_autoload');
