<?php

class XA_Tree_Menu {


	public static function getOutput($tree, $class = '', $name_field = 'name', $max_depth = null, $active = null) {
		if ($max_depth === 0) {
			return;
		}
		$return = '<ul class="'.$class.'">';
		$first = true;
		foreach ($tree as $node) {
			$item = $node['item'];
			$return .= '<li class="'.($item['content_id'] == $active ? ' active' : '').($first ? ' first' : '').'">'
				.'<a href="'.xa::url('path='.$item['path']).'">'.$item[$name_field].'</a>';
			if (!empty($node['children'])) {
				$return .= self::getOutput($node['children'], '', $name_field, ($max_depth !== null ? $max_depth - 1 : null));
			}
			$return .= '</li>';
			$first = false;
		}
		$return .= '</ul>';
		return $return;
	}
}
